package nl.plus.hanos.extendedparser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jxls.common.Context;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import nl.plus.hanos.extendedparser.dataobjecten.Algemeen;
import nl.plus.hanos.extendedparser.dataobjecten.Allergeen;
import nl.plus.hanos.extendedparser.dataobjecten.Allergenen;
import nl.plus.hanos.extendedparser.dataobjecten.ArtikelVariant;
import nl.plus.hanos.extendedparser.dataobjecten.BestelEenheid;
import nl.plus.hanos.extendedparser.dataobjecten.ConsumentenEenheid;
import nl.plus.hanos.extendedparser.dataobjecten.GTIN;
import nl.plus.hanos.extendedparser.dataobjecten.InkoopInfoRecord;
import nl.plus.hanos.extendedparser.dataobjecten.Keurmerk;
import nl.plus.hanos.extendedparser.dataobjecten.KeurmerkObject;
import nl.plus.hanos.extendedparser.dataobjecten.Logistiek;
import nl.plus.hanos.extendedparser.dataobjecten.Merk;
import nl.plus.hanos.extendedparser.dataobjecten.Overig;
import nl.plus.hanos.extendedparser.dataobjecten.Prijs;
import nl.plus.hanos.extendedparser.dataobjecten.Product;
import nl.plus.hanos.extendedparser.dataobjecten.ProductSpecifiek;
import nl.plus.hanos.extendedparser.dataobjecten.Voedingswaarde;
import nl.plus.hanos.extendedparser.dataobjecten.VoedingswaardeEntity;
import nl.plus.hanos.extendedparser.dataobjecten.VoedingswaardeList;
import nl.plus.hanos.extendedparser.dataobjecten.VoedingswaardeObject;
import nl.plus.hanos.extendedparser.jxls.JxlsHelper;

/**
 * Deze class werkt ALLEEN met de uitgeklede, ook wel extended XML genoemde, STEP-XML.
 * Een volledige XML laat deze applicatie klappen!
 */
public class STEPParser {

    private static Calendar start = Calendar.getInstance();
    private static Calendar inbetween;
    private Map<String, ArtikelVariant> artikelVariantMap = new HashMap<>();
    private Map<String, GTIN> gtinMap = new HashMap<>();
    private Map<String, Merk> merkMap = new HashMap<>();
    private Map<String, Allergeen> allergeenMap = new HashMap<>();
    private Map<String, VoedingswaardeEntity> voedingswaardeMap = new HashMap<>();
    private Map<String, Product> productMap = new HashMap<>();
    private Map<String, InkoopInfoRecord> inkoopInfoRecordMap = new HashMap<>();
    private Map<String, BestelEenheid> bestelEenheidMap = new HashMap<>();
    private Map<String, ConsumentenEenheid> consumentenEenheidMap = new HashMap<>();
    private Map<String, Keurmerk> keurmerkMap = new HashMap<>();

    private String masterDocNaam = "HANOS/HanosMasterDocument.xlsx";
    private String resultExcelNaamStart = "Hanos";
    private String bestandExtentie = ".xlsx";
    private String defaultDir = System.getProperty("user.dir");
    private String padNaarExcelFiles = "HANOS" + File.separator;
    private String userInfo = "Geen userinfo beschikbaar";
    private String absPath = "Geen path beschikbaar";

    public void parse(String payload) throws ParserConfigurationException, IOException, SAXException {
        //Build DOM

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = factory.newDocumentBuilder();
//        File file = new File(getClass().getClassLoader().getResource("1039-items.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("2-Items.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("1108-Items.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("onbekend.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("129-Items.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("500-items.xml").getFile());
//        File file = new File(getClass().getClassLoader().getResource("2-Items.xml").getFile());
        Document doc = builder.parse(new InputSource(new StringReader(payload)));

        XPathFactory xpathfactory = XPathFactory.newInstance();
        XPath xpath = xpathfactory.newXPath();

        try {
            XPathExpression expr = xpath.compile("//Asset");
            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList assetNodes = (NodeList) result;
            System.out.println("Going in to Asset loop. Found " + assetNodes.getLength() + " nodes.");
            // Er is een performance impact bij het iedere keer ophalen van de lengte van een NodeList.
            // vandaar een keer bepalen en dan die waarde gebruiken in de for loop.
            // Dit gebeurt meerdere keren in deze class.
            int assetNodeslength = assetNodes.getLength();
            for (int index = 0; index < assetNodeslength; index++) {

                Node assetNode = assetNodes.item(index);
                String userTypeIDValue = assetNode.getAttributes().getNamedItem("UserTypeID").getNodeValue();
                String nodeID = assetNode.getAttributes().getNamedItem("ID").getNodeValue();
                switch (userTypeIDValue) {
                    case "Logo":
                        Keurmerk keurmerk = new Keurmerk(nodeID);
                        Node anAssetNode = assetNode.getFirstChild();
                        while (anAssetNode.getLocalName() == null || !anAssetNode.getLocalName().equals("Values")) {
                            anAssetNode = anAssetNode.getNextSibling();
                        }
                        NodeList valueNodes = anAssetNode.getChildNodes();
                        int valueNodesLength = valueNodes.getLength();
                        for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
                            if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                                Element el = (Element) valueNodes.item(valueNodesCounter);
                                if (el.getNodeName().contains("Value")) {
                                    switch (el.getAttribute("AttributeID")) {
                                        case "LogoCode":
                                            keurmerk.setLogoCode(el.getFirstChild().getNodeValue());
                                            break;
                                        default:
                                            // Nog niets te doen hier. Wellicht in de toekomst
                                            break;
                                    }
                                }
                            }
                        }
                        keurmerkMap.put(nodeID, keurmerk);
                        break;
                    default:
                        System.out.println("Onbekend type: " + userTypeIDValue);
                        break;

                }
            }

            expr = xpath.compile("//Entity");
            result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList entityNodes = (NodeList) result;
            System.out.println("Going in to Entity loop. Found " + entityNodes.getLength() + " nodes.");
            for (int index = 0; index < entityNodes.getLength(); index++) {
                Node entityNode = entityNodes.item(index);
                String userTypeIDValue = entityNode.getAttributes().getNamedItem("UserTypeID").getNodeValue();
                String nodeID = entityNode.getAttributes().getNamedItem("ID").getNodeValue();
                switch (userTypeIDValue) {
                    case "ArtikelVariant":
                        ArtikelVariant av = new ArtikelVariant(nodeID);
                        Node anArtikelVariantNode = entityNode.getFirstChild();
                        while (anArtikelVariantNode.getLocalName() == null || !anArtikelVariantNode.getLocalName().equals("Values")) {
                            anArtikelVariantNode = anArtikelVariantNode.getNextSibling();
                        }
                        NodeList valueNodes = anArtikelVariantNode.getChildNodes();
                        int valueNodesLength = valueNodes.getLength();
                        for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
                            if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                                Element el = (Element) valueNodes.item(valueNodesCounter);
                                if (el.getNodeName().contains("Value")) {
                                    switch (el.getAttribute("AttributeID")) {
                                        case "ArtikelVariantCode":
                                            av.setArtikelVariantCode(el.getFirstChild().getNodeValue());
                                            break;
                                        default:
                                            // Nog niets te doen hier. Wellicht in de toekomst
                                            break;
                                    }
                                }
                            }
                        }
                        artikelVariantMap.put(nodeID, av);
                        break;
                    case "GTIN":
                        GTIN gtin = new GTIN(nodeID);
                        Node aGTINNode = entityNode.getFirstChild();
                        while (aGTINNode.getLocalName() == null || !aGTINNode.getLocalName().equals("Values")) {
                            aGTINNode = aGTINNode.getNextSibling();
                        }
                        NodeList gtinValueNodes = aGTINNode.getChildNodes();
                        int gtinValueNodesLength = gtinValueNodes.getLength();
                        for (int valueNodesCounter = 0; valueNodesCounter < gtinValueNodesLength; valueNodesCounter++) {
                            if (gtinValueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                                Element el = (Element) gtinValueNodes.item(valueNodesCounter);
                                if (el.getNodeName().contains("Value")) {
                                    switch (el.getAttribute("AttributeID")) {
                                        case "EANNr":
                                            gtin.setEanNr(el.getFirstChild().getNodeValue());
                                            break;
                                        case "PrimaireCodeIndicator":
                                            gtin.setPrimaireCodeIndicator(el.getFirstChild().getNodeValue());
                                            break;
                                        default:
                                            // Nog niets te doen hier. Wellicht in de toekomst
                                            break;
                                    }
                                }
                            }
                        }
                        gtinMap.put(nodeID, gtin);
                        break;
                    case "Merk":
                        Merk merk = new Merk(nodeID);
                        Node aMerkNode = entityNode.getFirstChild();
                        while (aMerkNode.getLocalName() == null || !aMerkNode.getLocalName().equals("Values")) {
                            aMerkNode = aMerkNode.getNextSibling();
                        }
                        NodeList merkValueNodes = aMerkNode.getChildNodes();
                        int merkValueNodesLength = merkValueNodes.getLength();
                        for (int valueNodesCounter = 0; valueNodesCounter < merkValueNodesLength; valueNodesCounter++) {
                            if (merkValueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                                Element el = (Element) merkValueNodes.item(valueNodesCounter);
                                if (el.getNodeName().contains("Value")) {
                                    switch (el.getAttribute("AttributeID")) {
                                        case "Omschrijving20":
                                            merk.setOmschrijving20(el.getFirstChild().getNodeValue());
                                            break;
                                        default:
                                            // Nog niets te doen hier. Wellicht in de toekomst
                                            break;
                                    }
                                }
                            }
                        }
                        merkMap.put(nodeID, merk);
                        break;
                    case "Allergeen":
                        Allergeen allergeen = new Allergeen(nodeID);
                        Node anAllergeenNode = entityNode.getFirstChild();
                        while (anAllergeenNode.getLocalName() == null || !anAllergeenNode.getLocalName().equals("Values")) {
                            anAllergeenNode = anAllergeenNode.getNextSibling();
                        }
                        NodeList allergeenValueNodes = anAllergeenNode.getChildNodes();
                        int allergeenValueNodesLength = allergeenValueNodes.getLength();
                        for (int valueNodesCounter = 0; valueNodesCounter < allergeenValueNodesLength; valueNodesCounter++) {
                            if (allergeenValueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                                Element el = (Element) allergeenValueNodes.item(valueNodesCounter);
                                if (el.getNodeName().contains("Value")) {
                                    switch (el.getAttribute("AttributeID")) {
                                        case "AllergeenCode":
                                            allergeen.setAllergeenCode(el.getFirstChild().getNodeValue());
                                            break;
                                        default:
                                            // Nog niets te doen hier. Wellicht in de toekomst
                                            break;
                                    }
                                }
                            }
                        }
                        allergeenMap.put(nodeID, allergeen);
                        break;
                    case "Voedingswaarde":
                        // Als het goed is heeft de Entity Voedingswaarde geen inhoud dus valt hier niets te doen.
                        break;
                    default:
                        System.out.println("Onbekend Type gevonden: " + userTypeIDValue);
                        break;
                }
            }

            expr = xpath.compile("//Product");
            result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList productNodes = (NodeList) result;
            System.out.println("Going in to Product loop. Found " + productNodes.getLength() + " nodes.");
            int productNodeLength = productNodes.getLength();
            for (int index = 0; index < productNodeLength; index++) {
                long calStart = Calendar.getInstance().getTimeInMillis();
                System.out.println("Node nummer: " + index);
                Node productNode = productNodes.item(index);
                String userTypeIDValue = productNode.getAttributes().getNamedItem("UserTypeID").getNodeValue();
                String nodeID = productNode.getAttributes().getNamedItem("ID").getNodeValue();
                switch (userTypeIDValue) {
                    case "Product":
                        Product product = new Product();
                        product.setId(nodeID);
                        Node aProductNode = productNode.getFirstChild();
                        while (aProductNode.getNextSibling() != null) {

                            if (aProductNode.getLocalName() == null) {
                                aProductNode = aProductNode.getNextSibling();
                            } else if (aProductNode.getLocalName().equals("EntityCrossReference")) {
                                behandelEntityCrossReference(product, aProductNode);
                                aProductNode = aProductNode.getNextSibling();
                            } else if (aProductNode.getLocalName().equals("AssetCrossReference")) {
                                behandelAssetCrossReference(product, aProductNode);
                                aProductNode = aProductNode.getNextSibling();
                            } else if (aProductNode.getLocalName().equals("Values")) {
                                behandelProductValues(product, aProductNode);
                                aProductNode = aProductNode.getNextSibling();
                            } else {
                                aProductNode = aProductNode.getNextSibling();
                            }
                        }
                        String tempString = "";
                        List<String> besteleenheidList = getBestelEenheidLijst(doc, xpath, "//Product[@ID='" + nodeID + "']/ProductCrossReference[@Type='WebUIPE-Product']");

                        product.setBesteleenheidId(besteleenheidList);
                        productMap.put(nodeID, product);
                        break;
                    case "BestelEenheid":
                        BestelEenheid bestelEenheid = new BestelEenheid(nodeID);
                        Node aBesteleenheidNode = productNode.getFirstChild();
                        while (aBesteleenheidNode.getNextSibling() != null) {

                            if (aBesteleenheidNode.getLocalName() == null) {
                                aBesteleenheidNode = aBesteleenheidNode.getNextSibling();
                            } else if (aBesteleenheidNode.getLocalName().equals("EntityCrossReference")) {
                                behandelBestelEenheidEntityCrossReference(bestelEenheid, aBesteleenheidNode);
                                aBesteleenheidNode = aBesteleenheidNode.getNextSibling();
                            } else if (aBesteleenheidNode.getLocalName().equals("ProductCrossReference")) {
                                behandelBestelEenheidProductCrossReference(bestelEenheid, aBesteleenheidNode);
                                aBesteleenheidNode = aBesteleenheidNode.getNextSibling();
                            } else if (aBesteleenheidNode.getLocalName().equals("Values")) {
                                behandelBestelEenheidProductValues(bestelEenheid, aBesteleenheidNode);
                                aBesteleenheidNode = aBesteleenheidNode.getNextSibling();
                            } else {
                                aBesteleenheidNode = aBesteleenheidNode.getNextSibling();
                            }
                        }
                        bestelEenheidMap.put(nodeID, bestelEenheid);
                        break;
                    case "ConsumentenEenheid":
                        ConsumentenEenheid consumentenEenheid = new ConsumentenEenheid(nodeID);
                        Node aConsumentenEenheidNode = productNode.getFirstChild();
                        while (aConsumentenEenheidNode.getNextSibling() != null) {

                            if (aConsumentenEenheidNode.getLocalName() == null) {
                                aConsumentenEenheidNode = aConsumentenEenheidNode.getNextSibling();
                            } else if (aConsumentenEenheidNode.getLocalName().equals("EntityCrossReference")) {
                                behandelConsumentenEenheidEntityCrossReference(consumentenEenheid, aConsumentenEenheidNode);
                                aConsumentenEenheidNode = aConsumentenEenheidNode.getNextSibling();
                            } else if (aConsumentenEenheidNode.getLocalName().equals("ProductCrossReference")) {
                                behandelConsumentenEenheidProductCrossReference(consumentenEenheid, aConsumentenEenheidNode);
                                aConsumentenEenheidNode = aConsumentenEenheidNode.getNextSibling();
                            } else if (aConsumentenEenheidNode.getLocalName().equals("Values")) {
                                behandelConsumentenEenheidProductValues(consumentenEenheid, aConsumentenEenheidNode);
                                aConsumentenEenheidNode = aConsumentenEenheidNode.getNextSibling();
                            } else {
                                aConsumentenEenheidNode = aConsumentenEenheidNode.getNextSibling();
                            }
                        }
                        consumentenEenheidMap.put(nodeID, consumentenEenheid);
                        break;
                    default:
                        System.out.println("Onbekend Type gevonden: " + userTypeIDValue);
                        break;
                }
                long calEnd = Calendar.getInstance().getTimeInMillis();
                System.out.println("Tijd (In milliseconden) benodigd voor 1 product: " + (calEnd - calStart));
            }
            System.out.println("Aantal ProductRecords: " + productMap.size());
            System.out.println("Aantal InkoopInfoRecords: " + inkoopInfoRecordMap.size());
            System.out.println("Aantal BestelEenheidRecords: " + bestelEenheidMap.size());
            System.out.println("Aantal ConsumentenEenheidRecords: " + consumentenEenheidMap.size());
            System.out.println("Aantal Varianten: " + artikelVariantMap.size());
            System.out.println("Aantal GTIN: " + gtinMap.size());
            System.out.println("Aantal Merken: " + merkMap.size());
            System.out.println("Aantal Allergenen: " + allergeenMap.size());
            System.out.println("Aantal Voedingswaarde: " + voedingswaardeMap.size());

            Set<String> ceKeys = consumentenEenheidMap.keySet();
            Iterator<String> ceKeysIterator = ceKeys.iterator();

            inbetween = Calendar.getInstance();
            // Maak alle benodigde verzamelingen aan
            List<Algemeen> algemeenCollectie = new ArrayList<>();
            List<Logistiek> logistiekCollectie = new ArrayList<>();
            List<Prijs> prijsCollectie = new ArrayList<>();
            List<VoedingswaardeObject> voedingswaardeCollectie = new ArrayList<>();
            List<Allergenen> allergenenCollectie = new ArrayList<>();
            List<ProductSpecifiek> productSpecifiekCollectie = new ArrayList<>();
            List<KeurmerkObject> keurmerkenCollectie = new ArrayList<>();
            List<Overig> overigCollectie = new ArrayList<>();
            while (ceKeysIterator.hasNext()) {
                String key = ceKeysIterator.next();
                ConsumentenEenheid ce = consumentenEenheidMap.get(key);
                ArtikelVariant av = artikelVariantMap.get(ce.getArtikelVariantID());
                boolean isSTDVariant = false;
                if (av != null) {
                    isSTDVariant = artikelVariantMap.get(ce.getArtikelVariantID()).getArtikelVariantCode().equals("STD");
                }
                boolean isPrimaireCodeIndcator = ce.getEanCodeCE() != null;
                boolean hasProductReference = ce.getProductID().length() > 0;

                if (isSTDVariant && isPrimaireCodeIndcator && hasProductReference) {
                    BestelEenheid bijbehorendeBestelEenheid = getBestelEenheid(ce, bestelEenheidMap);
                    String naam = productMap.get(ce.getProductID()).getName();
                    String merkId = productMap.get(ce.getProductID()).getMerkId();
                    Algemeen algemeen = new Algemeen(
                            bijbehorendeBestelEenheid.getEanCodePE(),
                            ce.getEanCodeCE(),
                            "Nee",
                            naam,
                            "",
                            "",
                            "",
                            productMap.get(ce.getProductID()).getVerpakkingVanHetProductArtikel(),
                            productMap.get(ce.getProductID()).getInhoudVanHetProduct(),
                            productMap.get(ce.getProductID()).getEenheidVanHetProduct(),
                            "",
                            productMap.get(ce.getProductID()).getStatGoederennrImportcodenr(),
                            merkMap.get(merkId).getOmschrijving20()
                    );
                    algemeenCollectie.add(algemeen);
                    Logistiek logistiek = new Logistiek(
                            naam,
                            ce.getCeBrutoGewicht(),
                            ce.getCeNettoGewicht(),
                            ce.getCeLengte(),
                            ce.getCeBreedte(),
                            ce.getCeHoogte(),
                            bijbehorendeBestelEenheid.getAantalCEPerHE(),
                            bijbehorendeBestelEenheid.getEanCodePE(),
                            bijbehorendeBestelEenheid.getHeBrutoGewichtInGram(),
                            bijbehorendeBestelEenheid.getHeLengteInMm(),
                            bijbehorendeBestelEenheid.getHeBreedteInMm(),
                            bijbehorendeBestelEenheid.getHeHoogteInMm(),
                            1,
                            1,
                            bijbehorendeBestelEenheid.getMinimaleBestelhoeveelheidInCE()
                    );
                    logistiekCollectie.add(logistiek);
                    Overig overig = new Overig(
                            naam,
                            productMap.get(ce.getProductID()).getHanosIndicator(),
                            productMap.get(ce.getProductID()).getArtikelStatus(),
                            ce.getCeArtikelStatus(),
                            bijbehorendeBestelEenheid.getArtikelStatusPE(),
                            ce.getCeNummer()
                    );
                    overigCollectie.add(overig);
                    ProductSpecifiek prodSPecifiek = new ProductSpecifiek(
                            naam,
                            productMap.get(ce.getProductID()).getAlcoholPercentage(),
                            "",
                            "",
                            "",
                            productMap.get(ce.getProductID()).getVangstGebied(),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                    );
                    productSpecifiekCollectie.add(prodSPecifiek);
                    VoedingswaardeList vwList = productMap.get(ce.getProductID()).getVoedingswaardeVerzameling();
                    VoedingswaardeObject voedingsWaarden = new VoedingswaardeObject(
                            naam,
                            productMap.get(ce.getProductID()).getThtTermijnWinkelInDagen(),
                            productMap.get(ce.getProductID()).getThtTermijnInDagen(),
                            vwList.getVoedingswaardeValueByKey("ENER-KJ"),
                            vwList.getVoedingswaardeValueByKey("ENER-KC"),
                            vwList.getVoedingswaardeValueByKey("FAT"),
                            vwList.getVoedingswaardeValueByKey("FASAT"),
                            vwList.getVoedingswaardeValueByKey("FAMSCIS"),
                            vwList.getVoedingswaardeValueByKey("FAPUCIS"),
                            vwList.getVoedingswaardeValueByKey("CHOAVL"),
                            vwList.getVoedingswaardeValueByKey("SUGAR-"),
                            vwList.getVoedingswaardeValueByKey("PRO-"),
                            vwList.getVoedingswaardeValueByKey("SALTEQ"),
                            vwList.getVoedingswaardeValueByKey("FIBTG"),
                            vwList.getVoedingswaardeValueByKey("POLYL"),
                            vwList.getVoedingswaardeValueByKey("STARCH"),
                            vwList.getVoedingswaardeValueByKey("CHOL"),
                            productMap.get(ce.getProductID()).getIngredienten(),
                            productMap.get(ce.getProductID()).getBewaarAdviesMinimaleTemperatuur(),
                            productMap.get(ce.getProductID()).getBewaarAdviesMaximaleTemperatuur(),
                            productMap.get(ce.getProductID()).getBewaarinstructies(),
                            productMap.get(ce.getProductID()).getBereidingsWijze2500()
                    );
                    voedingswaardeCollectie.add(voedingsWaarden);
                    List<String> allergeenCodes = productMap.get(ce.getProductID()).getAllergeenVerzameling();
                    Allergenen allergenen = new Allergenen(naam,
                            (allergeenCodes.contains("AW") || allergeenCodes.contains("AX")) ? "Met" : "Onbekend", // Gluten
                            (allergeenCodes.contains("AC") || allergeenCodes.contains("UN")) ? "Met" : "Onbekend", // Schaaldieren
                            (allergeenCodes.contains("AE")) ? "Met" : "Onbekend", // Ei.
                            (allergeenCodes.contains("AF")) ? "Met" : "Onbekend", // vis,
                            (allergeenCodes.contains("AP")) ? "Met" : "Onbekend", // aardnoten,
                            (allergeenCodes.contains("AY")) ? "Met" : "Onbekend", // soja,
                            (allergeenCodes.contains("AM")) ? "Met" : "Onbekend", // melk,
                            (allergeenCodes.contains("AN")) ? "Met" : "Onbekend", // noten,
                            (allergeenCodes.contains("BC")) ? "Met" : "Onbekend", // selderij,
                            (allergeenCodes.contains("BM")) ? "Met" : "Onbekend", // mosterd,
                            (allergeenCodes.contains("AS")) ? "Met" : "Onbekend", // sesam,
                            (allergeenCodes.contains("AU")) ? "Met" : "Onbekend", // zwafelEnSulfieten,
                            (allergeenCodes.contains("NL")) ? "Met" : "Onbekend", // lupine,
                            (allergeenCodes.contains("UM")) ? "Met" : "Onbekend" // weekdieren
                    );
                    allergenenCollectie.add(allergenen);

                    KeurmerkObject keurmerkenObject = new KeurmerkObject();
                    if (keurmerkMap != null && keurmerkMap.size() > 0) {
                        List<String> geselecteerdeKeurmerken = productMap.get(ce.getProductID()).getKeurmerkenLijst();

                        if(geselecteerdeKeurmerken != null) {
                            for (int index = 0; index < geselecteerdeKeurmerken.size(); index++) {
                                keurmerkenObject.setProductNaam(naam);
                                switch (index) {
                                    case 0:
                                        keurmerkenObject.setKeurmerk1(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 1:
                                        keurmerkenObject.setKeurmerk2(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 2:
                                        keurmerkenObject.setKeurmerk3(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 3:
                                        keurmerkenObject.setKeurmerk4(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 4:
                                        keurmerkenObject.setKeurmerk5(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 5:
                                        keurmerkenObject.setKeurmerk6(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    case 6:
                                        keurmerkenObject.setKeurmerk7(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        break;
                                    default:
                                        if (keurmerkenObject.getOverigeKeurmerken() == null) {
                                            keurmerkenObject.setOverigeKeurmerken(keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        } else {
                                            keurmerkenObject.setOverigeKeurmerken(keurmerkenObject.getOverigeKeurmerken() + ", " + keurmerkMap.get(geselecteerdeKeurmerken.get(index)).getLogoCode());
                                        }
                                        break;

                                }
                            }
                        }
                        keurmerkenCollectie.add(keurmerkenObject);
                    }
                }
            }

            // Nu de Excel inlezen en XML data integreren
            createExcel(algemeenCollectie, prijsCollectie, logistiekCollectie, voedingswaardeCollectie, allergenenCollectie, productSpecifiekCollectie, keurmerkenCollectie, overigCollectie);

            // Initieer alle verzamelingen omdat ze anders wellicht worden aangevuld ipv opnieuw gevuld. There is a difference!!!
            algemeenCollectie = new ArrayList<>();
            logistiekCollectie = new ArrayList<>();
            voedingswaardeCollectie = new ArrayList<>();
            allergenenCollectie = new ArrayList<>();
            productSpecifiekCollectie = new ArrayList<>();
            keurmerkenCollectie = new ArrayList<>();
            overigCollectie = new ArrayList<>();

            // @TODO Niet zeker of dit nodig is dus nu nog even laten staan maar later testen en verwijderen.
            clearMaps();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }


    }

    private void behandelProductValues(Product product, Node aProductNode) {
        NodeList valueNodes = aProductNode.getChildNodes();
        int valueNodesLength = valueNodes.getLength();
        for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
            if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) valueNodes.item(valueNodesCounter);
                if (el.getNodeName().equals("Value")) {
                    switch (el.getAttribute("AttributeID")) {
                        case "VerpakkingVanHetProductArtikel":
                            product.setVerpakkingVanHetProductArtikel(el.getAttribute("ID"));
                            break;
                        case "THTTermijnInDagen":
                            String tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                product.setThtTermijnInDagen(Integer.parseInt(tempString));
                            }
                            break;
                        case "THTTermijnWinkelInDagen":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                product.setThtTermijnWinkelInDagen(Integer.parseInt(tempString));
                            }
                            break;
                        case "InhoudVanHetProduct":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                product.setInhoudVanHetProduct(Integer.parseInt(tempString));
                            }
                            break;
                        case "EenheidVanHetProduct":
                            product.setEenheidVanHetProduct(el.getFirstChild().getNodeValue());
                            break;
                        case "ProductOmschrijving":
                            product.setName(el.getFirstChild().getNodeValue());
                            break;
                        case "AlcoholPercentage":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                product.setAlcoholPercentage(Double.parseDouble(tempString));
                            }
                            break;
                        case "Ingredienten":
                            product.setIngredienten(el.getFirstChild().getNodeValue());
                            break;
                        case "TemperatuurrangeBijOntvanst":
                            tempString = el.getAttribute("ID");
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                product.setTemperatuurrangeBijOntvangst(Integer.parseInt(tempString));
                            }
                            break;
                        case "Bewaarinstructies":
                            product.setBewaarinstructies(el.getFirstChild().getNodeValue());
                            break;
                        case "BereidingsWijze2500":
                            product.setBereidingsWijze2500(el.getFirstChild().getNodeValue());
                            break;
                        case "GS1-plac0024":
                            product.setGs1plac0024(el.getFirstChild().getNodeValue());
                            break;
                        case "IntrastatNummer":
                            product.setStatGoederennrImportcodenr(el.getFirstChild().getNodeValue());
                            break;
                        case "ArtikelStatus":
                            product.setArtikelStatus(el.getFirstChild().getNodeValue());
                            break;
                        default:
                            // Nog niets te doen hier. Welicht in de toekomst.
                            break;
                    }
                } else if (el.getNodeName().equals("MultiValue")) {
                    Node multiValueNode = el.getFirstChild().getNextSibling();
                    if (multiValueNode.getAttributes().getNamedItem("ID").getNodeValue().equals("HANOS")) {
                        product.setHanosIndicator(multiValueNode.getFirstChild().getNodeValue());
                    }

                }
            }

        }
    }

    private void behandelBestelEenheidProductValues(BestelEenheid bestelEenheid, Node aBestelEenheidNode) {
        NodeList valueNodes = aBestelEenheidNode.getChildNodes();
        int valueNodesLength = valueNodes.getLength();
        for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
            if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) valueNodes.item(valueNodesCounter);
                if (el.getNodeName().equals("Value")) {
                    switch (el.getAttribute("AttributeID")) {
                        case "Diepte":
                            String eenheid = el.getAttribute("UnitID");
                            String tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                bestelEenheid.setHeLengteInMm(theValue);
                            }
                            break;
                        case "Breedte":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                bestelEenheid.setHeBreedteInMm(theValue);
                            }
                            break;
                        case "ConsumentenEenheidArtikelNummer":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                //bestelEenheid.set(Integer.parseInt(tempString));
                            }
                            break;
                        case "ArtikelStatus":
                            bestelEenheid.setArtikelStatusPE(el.getFirstChild().getNodeValue());
                            break;
                        case "AantalCeInPe":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                bestelEenheid.setAantalCEPerHE(Integer.parseInt(tempString));
                                bestelEenheid.setMinimaleBestelhoeveelheidInCE(bestelEenheid.getAantalCEPerHE());
                            }
                            break;
                        case "GewichtColloInKg":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                bestelEenheid.setHeBrutoGewichtInGram(Double.parseDouble(tempString) * 1000);
                            }
                            break;
                        case "Hoogte":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                bestelEenheid.setHeHoogteInMm(theValue);
                            }
                            break;
                        default:
                            // Nog niets te doen hier. Welicht in de toekomst.
                            break;
                    }
                }
            }

        }
    }

    private void behandelConsumentenEenheidProductValues(ConsumentenEenheid consumentenEenheid, Node aConsumentenEenheidNode) {
        NodeList valueNodes = aConsumentenEenheidNode.getChildNodes();
        int valueNodesLength = valueNodes.getLength();
        for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
            if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) valueNodes.item(valueNodesCounter);
                if (el.getNodeName().equals("Value")) {
                    switch (el.getAttribute("AttributeID")) {
                        case "Diepte":
                            String eenheid = el.getAttribute("UnitID");
                            String tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                consumentenEenheid.setCeLengte(theValue);
                            }
                            break;
                        case "Breedte":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                consumentenEenheid.setCeBreedte(theValue);
                            }
                            break;
                        case "Hoogte":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("CMT")) {
                                    theValue *= 10;
                                }
                                consumentenEenheid.setCeHoogte(theValue);
                            }
                            break;
                        case "PrimaireCodeIndicator":
                            // Doen we niets mee op deze plek.
                            break;
                        case "ArtikelStatus":
                            consumentenEenheid.setCeArtikelStatus(el.getFirstChild().getNodeValue());
                            break;
                        case "ConsumentenEenheidArtikelNummer":
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*")) {
                                consumentenEenheid.setCeNummer(el.getFirstChild().getNodeValue());
                            }
                            break;
                        case "BrutoGewicht":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("KGM")) {
                                    theValue *= 1000;
                                }
                                consumentenEenheid.setCeBrutoGewicht(theValue);
                            }
                            break;
                        case "NettoGewicht":
                            eenheid = el.getAttribute("UnitID");
                            tempString = el.getFirstChild().getNodeValue();
                            if (tempString != null && tempString.length() > 0 && tempString.matches("\\d*\\.\\d*|\\d*")) {
                                Double theValue = Double.parseDouble(tempString);
                                if (eenheid.endsWith("KGM")) {
                                    theValue *= 1000;
                                }
                                consumentenEenheid.setCeNettoGewicht(theValue);
                            }
                            break;
                        default:
                            // Nog niets te doen hier. Welicht in de toekomst.
                            break;
                    }
                }
            }

        }
    }

    private void behandelAssetCrossReference(Product product, Node aProductNode) {
        if (aProductNode.getAttributes().getNamedItem("Type").getNodeValue().equals("Logo")) {
            product.addKeurmerk(aProductNode.getAttributes().getNamedItem("AssetID").getNodeValue());
        }
    }

    private void behandelBestelEenheidProductCrossReference(BestelEenheid bestelEenheid, Node aBestelEenheidNode) {
        if (aBestelEenheidNode.getAttributes().getNamedItem("Type").getNodeValue().equals("ConsumentenEenheid")) {
            bestelEenheid.addConsumentenEenheidID(aBestelEenheidNode.getAttributes().getNamedItem("ProductID").getNodeValue());
        }
    }

    private void behandelConsumentenEenheidProductCrossReference(ConsumentenEenheid consumentenEenheid, Node aConsumentenEenheidNode) {
        if (aConsumentenEenheidNode.getAttributes().getNamedItem("Type").getNodeValue().equals("BaseUnit2Product")) {
            consumentenEenheid.setProductID(aConsumentenEenheidNode.getAttributes().getNamedItem("ProductID").getNodeValue());
        }
    }

    private void behandelEntityCrossReference(Product product, Node aProductNode) {
        if (aProductNode.getAttributes().getNamedItem("Type").getNodeValue().equals("Merk")) {
            product.setMerkId(aProductNode.getAttributes().getNamedItem("EntityID").getNodeValue());

        } else if (aProductNode.getAttributes().getNamedItem("Type").getNodeValue().equals("AllergeenBevat")) {
            String allergenenKey = aProductNode.getAttributes().getNamedItem("EntityID").getNodeValue();
            product.addAllergeen(allergeenMap.get(allergenenKey).getAllergeenCode());
        } else if (aProductNode.getAttributes().getNamedItem("Type").getNodeValue().equals("Voedingswaardes")) {
            String voedingswaardeID = aProductNode.getAttributes().getNamedItem("EntityID").getNodeValue();
            Voedingswaarde vw = new Voedingswaarde(voedingswaardeID);

            while (aProductNode.getLocalName() == null || !aProductNode.getLocalName().equals("MetaData")) {
                aProductNode = aProductNode.getFirstChild().getNextSibling();
            }

            NodeList valueNodes = aProductNode.getChildNodes();
            int valueNodesLength = valueNodes.getLength();
            for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
                if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) valueNodes.item(valueNodesCounter);
                    if (el.getNodeName().contains("Value")) {
                        switch (el.getAttribute("AttributeID")) {
                            case "Voedingswaarde":
                                String tempString = el.getFirstChild().getNodeValue();
                                if (tempString != null && tempString.matches("\\d*\\.\\d*")) {
                                    vw.setVoedingswaarde(Double.parseDouble(tempString));
                                }
                                break;
                            case "EenheidVoedingswaarde":
                                vw.setEenheid(el.getFirstChild().getNodeValue());
                                break;
                            case "SortVoedingswaardenCalc":
                                tempString = el.getFirstChild().getNodeValue();
                                if (tempString != null && tempString.matches("\\d*")) {
                                    vw.setSortVoedingswaardenCalc(Integer.parseInt(tempString));
                                }
                                break;
                            default:
                                // Nog niets te doen hier
                                break;
                        }
                    }
                }
            }
            product.addVoedingswaarde(vw);
        }
    }

    private void behandelBestelEenheidEntityCrossReference(BestelEenheid bestelEenheid, Node aBestelEenheidNode) {
        if (aBestelEenheidNode.getAttributes().getNamedItem("Type").getNodeValue().equals("GTIN")) {
            String gtinID = aBestelEenheidNode.getAttributes().getNamedItem("EntityID").getNodeValue();

            while (aBestelEenheidNode.getLocalName() == null || !aBestelEenheidNode.getLocalName().equals("MetaData")) {
                aBestelEenheidNode = aBestelEenheidNode.getFirstChild().getNextSibling();
            }

            NodeList valueNodes = aBestelEenheidNode.getChildNodes();
            int valueNodesLength = valueNodes.getLength();
            for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
                if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) valueNodes.item(valueNodesCounter);
                    if (el.getNodeName().contains("Value")) {
                        switch (el.getAttribute("AttributeID")) {
                            case "PrimaireCodeIndicator":
                                if (el.getAttribute("ID").equals("J")) {
                                    bestelEenheid.setEanCodePE(gtinMap.get(gtinID).getEanNr());
                                }
                                break;
                            default:
                                // Nog niets te doen hier
                                break;
                        }
                    }
                }
            }
        }
    }

    private void behandelConsumentenEenheidEntityCrossReference(ConsumentenEenheid consumentenEenheid, Node aConsumentenEenheidNode) {
        if (aConsumentenEenheidNode.getAttributes().getNamedItem("Type").getNodeValue().equals("GTIN")) {
            String gtinID = aConsumentenEenheidNode.getAttributes().getNamedItem("EntityID").getNodeValue();

            while (aConsumentenEenheidNode.getLocalName() == null || !aConsumentenEenheidNode.getLocalName().equals("MetaData")) {
                aConsumentenEenheidNode = aConsumentenEenheidNode.getFirstChild().getNextSibling();
            }

            NodeList valueNodes = aConsumentenEenheidNode.getChildNodes();
            int valueNodesLength = valueNodes.getLength();
            for (int valueNodesCounter = 0; valueNodesCounter < valueNodesLength; valueNodesCounter++) {
                if (valueNodes.item(valueNodesCounter).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) valueNodes.item(valueNodesCounter);
                    if (el.getNodeName().contains("Value")) {
                        switch (el.getAttribute("AttributeID")) {
                            case "PrimaireCodeIndicator":
                                if (el.getAttribute("ID").equals("J")) {
                                    consumentenEenheid.setEanCodeCE(gtinMap.get(gtinID).getEanNr());
                                }
                                break;
                            default:
                                // Nog niets te doen hier
                                break;
                        }
                    }
                }
            }
        } else if (aConsumentenEenheidNode.getAttributes().getNamedItem("Type").getNodeValue().equals("ArtikelVariantType")) {
            consumentenEenheid.setArtikelVariantID(aConsumentenEenheidNode.getAttributes().getNamedItem("EntityID").getNodeValue());
        }
    }

    public void main(String payload) throws IOException, SAXException, ParserConfigurationException {
        STEPParser sp = new STEPParser();
        sp.parse(payload);
        Calendar stop = Calendar.getInstance();

        System.out.println("Gebruikte Tijd voor inlezen XML: " + (inbetween.getTimeInMillis() - start.getTimeInMillis()));
        System.out.println("Gebruikte Tijd voor maken Excel: " + (stop.getTimeInMillis() - inbetween.getTimeInMillis()));
        System.out.println("Totaal Gebruikte Tijd: " + (stop.getTimeInMillis() - start.getTimeInMillis()));
    }

    private List<String> getBestelEenheidLijst(Document doc, XPath xpath, String path) throws XPathExpressionException {
        List<String> theList = new ArrayList<>();
        XPathExpression expr = xpath.compile(path);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        for (int index = 0; index < nodes.getLength(); index++) {
            String bestelEenheidId = nodes.item(index).getAttributes().getNamedItem("ProductID").getNodeValue();
            theList.add(bestelEenheidId);
        }
        return theList;
    }

    private BestelEenheid getBestelEenheid(ConsumentenEenheid ce, Map<String, BestelEenheid> bestelEenhedenMap) {
        Set<String> bestelEenheidKeys = bestelEenhedenMap.keySet();
        Iterator<String> bestelEenheidKeysIterator = bestelEenheidKeys.iterator();
        while (bestelEenheidKeysIterator.hasNext()) {
            String key = bestelEenheidKeysIterator.next();
            BestelEenheid be = bestelEenhedenMap.get(key);
            List<String> ceIDs = be.getConsumentenEenheidIDs();
            if (ceIDs.contains(ce.getConsumentenEenheidId())) {
                return be;
            }
        }
        return null;
    }

    private void createExcel(List<Algemeen> algemeenCollectie, List<Prijs> prijsCollectie, List<Logistiek> logistiekCollectie, List<VoedingswaardeObject> voedingswaardeCollectie,
                             List<Allergenen> allergenenCollectie, List<ProductSpecifiek> productSpecifiekCollectie, List<KeurmerkObject> keurmerkenCollectie,
                             List<Overig> overigCollectie) { // throws MbUserException {
        try {
            if (checkVerzamelingen(algemeenCollectie, logistiekCollectie, voedingswaardeCollectie, allergenenCollectie,
                    productSpecifiekCollectie, keurmerkenCollectie, overigCollectie)) {
                File file2 = new File(getClass().getClassLoader().getResource("HANOS/HanosMasterDocument.xlsx").getFile());
                URL fileResource = file2.toURL();
                if (fileResource == null) {
//                    MbUserException mbue = new MbUserException(this, "createExcel()", userInfo, absPath, "file not found @ " + defaultDir, null);
//                    throw mbue;
                    System.out.println("Iets met de HanosMaster file");
                }
//                userInfo = fileResource.getUserInfo();
                File file = new File(fileResource.toURI());
                absPath = file.getAbsolutePath();
                InputStream is = new FileInputStream(file);
                // Om bestandsnaam redelijk uniek te maken voegen we een datum/tijd toe aan bestandsnaam
                String datumTijdString = maakDatumTijdString();
                File output = new File("/Users/jenswitteveen/Documents/" + resultExcelNaamStart + datumTijdString + bestandExtentie);
                OutputStream os = new FileOutputStream(output);
                Context context = new Context();
                context.putVar("algemeenVerzameling", algemeenCollectie);
                context.putVar("prijsVerzameling", prijsCollectie);
                context.putVar("logistiekVerzameling", logistiekCollectie);
                context.putVar("voedingswaardeVerzameling", voedingswaardeCollectie);
                context.putVar("allergenenVerzameling", allergenenCollectie);
                context.putVar("productSpecifiekVerzameling", productSpecifiekCollectie);
                context.putVar("keurmerkVerzameling", keurmerkenCollectie);
                context.putVar("overigVerzameling", overigCollectie);
                context.putVar("sheetNames", Arrays.asList(
                        "Algemeen",
                        "Prijs",
                        "Logistiek",
                        "Voedingswaarden",
                        "Allergenen",
                        "Product spec.",
                        "Keurmerken",
                        "Additioneel"
                ));
                JxlsHelper.getInstance().processTemplate(is, os, context);
                System.out.println("file wordt weggeschreven naar " + output.getAbsolutePath());
                is.close();
                os.flush();
                os.close();
            } else {
                // De verzamelingen zijn niet gevuld. Daarom niets te processen
            }

        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    private String maakDatumTijdString() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
                + cal.get(Calendar.YEAR) + "--" + cal.get(Calendar.HOUR_OF_DAY) + "-" + cal.get(Calendar.MINUTE);
    }

    private boolean checkVerzamelingen(List<Algemeen> algemeenCollectie, List<Logistiek> logistiekCollectie, List<VoedingswaardeObject> voedingswaardeCollectie,
                                       List<Allergenen> allergenenCollectie, List<ProductSpecifiek> productSpecifiekCollectie, List<KeurmerkObject> keurmerkenCollectie,
                                       List<Overig> overigCollectie) {
        if (isNotNullAndNotEmpty(algemeenCollectie) && isNotNullAndNotEmpty(logistiekCollectie)
                && isNotNullAndNotEmpty(voedingswaardeCollectie) && isNotNullAndNotEmpty(allergenenCollectie)
                && isNotNullAndNotEmpty(productSpecifiekCollectie)
                && isNotNullAndNotEmpty(overigCollectie)) {
            return true;
        }
        return false;
    }

    private boolean isNotNullAndNotEmpty(Collection<?> aCollection) {
        return aCollection != null && !aCollection.isEmpty();
    }

    private void clearMaps() {
        artikelVariantMap = new HashMap<>();
        gtinMap = new HashMap<>();
        merkMap = new HashMap<>();
        allergeenMap = new HashMap<>();
        voedingswaardeMap = new HashMap<>();
        productMap = new HashMap<>();
        inkoopInfoRecordMap = new HashMap<>();
        bestelEenheidMap = new HashMap<>();
        consumentenEenheidMap = new HashMap<>();
    }
}
