package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor keurmerk informatie over een artikel
public class KeurmerkObject {
 private String productNaam = "";
 private String keurmerk1;
 private String keurmerk2;
 private String keurmerk3;
 private String keurmerk4;
 private String keurmerk5;
 private String keurmerk6;
 private String keurmerk7;
 private String overigeKeurmerken;

    public KeurmerkObject() {
    }

    public KeurmerkObject(String productNaam, String keurmerk1, String keurmerk2, String keurmerk3, String keurmerk4,
                          String keurmerk5, String keurmerk6, String keurmerk7, String overigeKeurmerken) {
     this.productNaam = productNaam;
     this.keurmerk1 = keurmerk1;
     this.keurmerk2 = keurmerk2;
     this.keurmerk3 = keurmerk3;
     this.keurmerk4 = keurmerk4;
     this.keurmerk5 = keurmerk5;
     this.keurmerk6 = keurmerk6;
     this.keurmerk7 = keurmerk7;
     this.overigeKeurmerken = overigeKeurmerken;
 }

 public String getProductNaam() {
     return productNaam;
 }

 public void setProductNaam(String productNaam) {
     this.productNaam = productNaam;
 }

 public String getKeurmerk1() {
     return keurmerk1;
 }

 public void setKeurmerk1(String keurmerk1) {
     this.keurmerk1 = keurmerk1;
 }

 public String getKeurmerk2() {
     return keurmerk2;
 }

 public void setKeurmerk2(String keurmerk2) {
     this.keurmerk2 = keurmerk2;
 }

 public String getKeurmerk3() {
     return keurmerk3;
 }

 public void setKeurmerk3(String keurmerk3) {
     this.keurmerk3 = keurmerk3;
 }

 public String getKeurmerk4() {
     return keurmerk4;
 }

 public void setKeurmerk4(String keurmerk4) {
     this.keurmerk4 = keurmerk4;
 }

 public String getKeurmerk5() {
     return keurmerk5;
 }

 public void setKeurmerk5(String keurmerk5) {
     this.keurmerk5 = keurmerk5;
 }

 public String getKeurmerk6() {
     return keurmerk6;
 }

 public void setKeurmerk6(String keurmerk6) {
     this.keurmerk6 = keurmerk6;
 }

 public String getKeurmerk7() {
     return keurmerk7;
 }

 public void setKeurmerk7(String keurmerk7) {
     this.keurmerk7 = keurmerk7;
 }

 public String getOverigeKeurmerken() {
     return overigeKeurmerken;
 }

 public void setOverigeKeurmerken(String overigeKeurmerken) {
     this.overigeKeurmerken = overigeKeurmerken;
 }

 @Override
 public String toString() {
     return "Keurmerk{" +
    		 "ProductNaam='" + productNaam + "'" +
             "keurmerk1='" + keurmerk1 + "'" +
             ", keurmerk2='" + keurmerk2 + "'" +
             ", keurmerk3='" + keurmerk3 + "'" +
             ", keurmerk4='" + keurmerk4 + "'" +
             ", keurmerk5='" + keurmerk5 + "'" +
             ", keurmerk6='" + keurmerk6 + "'" +
             ", keurmerk7='" + keurmerk7 + "'" +
             ", overigeKeurmerken='" + overigeKeurmerken + "'" +
             "}";
 }
}
