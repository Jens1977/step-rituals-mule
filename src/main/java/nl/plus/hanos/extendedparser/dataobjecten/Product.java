package nl.plus.hanos.extendedparser.dataobjecten;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Product {
    private String id;
    private String name;
    private List<String> besteleenheidId;
    private List<String> inkoopInfoRecordId;
    private VoedingswaardeList voedingswaardeVerzameling = new VoedingswaardeList();
    private String merkId;
    private List<String> allergeenVerzameling = new ArrayList<>();
    private String verpakkingVanHetProductArtikel;
    private Integer thtTermijnInDagen;
    private Integer thtTermijnWinkelInDagen;
    private Integer inhoudVanHetProduct;
    private String eenheidVanHetProduct;
    private Double alcoholPercentage;
    private String ingredienten;
    private Integer temperatuurrangeBijOntvangst;
    private String bewaarinstructies;
    private String bereidingsWijze2500;
    private String gs1plac0024;
    private String artikelStatus;
    private String vangstGebied;
    private String statGoederennrImportcodenr;
    private String hanosIndicator;
    private List<String> keurmerkenLijst;

    public Product() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getBesteleenheidId() {
        return besteleenheidId;
    }

    public void setBesteleenheidId(List<String> besteleenheidId) {
        this.besteleenheidId = besteleenheidId;
    }

    public List<String> getInkoopInfoRecordId() {
        return inkoopInfoRecordId;
    }

    public void setInkoopInfoRecordId(List<String> inkoopInfoRecordId) {
        this.inkoopInfoRecordId = inkoopInfoRecordId;
    }

    public VoedingswaardeList getVoedingswaardeVerzameling() {
        return voedingswaardeVerzameling;
    }

    public void setVoedingswaardeVerzameling(VoedingswaardeList voedingswaardeVerzameling) {
        this.voedingswaardeVerzameling = voedingswaardeVerzameling;
    }

    public void addVoedingswaarde(Voedingswaarde voedingswaarde) {
        if (voedingswaardeVerzameling ==null) {
            voedingswaardeVerzameling = new VoedingswaardeList();
        }
        voedingswaardeVerzameling.addVoedingswaarde(voedingswaarde);
    }

    public String getMerkId() {
        return merkId;
    }

    public void setMerkId(String merkId) {
        this.merkId = merkId;
    }

    public List<String> getAllergeenVerzameling() {
        return allergeenVerzameling;
    }

    public void addAllergeen(String allergeenID) {
        if (allergeenVerzameling == null) {
            allergeenVerzameling = new ArrayList<>();
        }
        allergeenVerzameling.add(allergeenID);
    }

    public String getVerpakkingVanHetProductArtikel() {
        return verpakkingVanHetProductArtikel;
    }

    public void setVerpakkingVanHetProductArtikel(String verpakkingVanHetProductArtikel) {
        this.verpakkingVanHetProductArtikel = verpakkingVanHetProductArtikel;
    }

    public Integer getThtTermijnInDagen() {
        return thtTermijnInDagen;
    }

    public void setThtTermijnInDagen(Integer thtTermijnInDagen) {
        this.thtTermijnInDagen = thtTermijnInDagen;
    }

    public Integer getThtTermijnWinkelInDagen() {
        return thtTermijnWinkelInDagen;
    }

    public void setThtTermijnWinkelInDagen(Integer thtTermijnWinkelInDagen) {
        this.thtTermijnWinkelInDagen = thtTermijnWinkelInDagen;
    }

    public Integer getInhoudVanHetProduct() {
        return inhoudVanHetProduct;
    }

    public void setInhoudVanHetProduct(Integer inhoudVanHetProduct) {
        this.inhoudVanHetProduct = inhoudVanHetProduct;
    }

    public String getEenheidVanHetProduct() {
        return eenheidVanHetProduct;
    }

    public void setEenheidVanHetProduct(String eenheidVanHetProduct) {
        this.eenheidVanHetProduct = eenheidVanHetProduct;
    }

    public Double getAlcoholPercentage() {
        return alcoholPercentage;
    }

    public void setAlcoholPercentage(Double alcoholPercentage) {
        this.alcoholPercentage = alcoholPercentage;
    }

    public String getIngredienten() {
        return ingredienten;
    }

    public void setIngredienten(String ingredienten) {
        this.ingredienten = ingredienten;
    }

    public Integer getTemperatuurrangeBijOntvangst() {
        return temperatuurrangeBijOntvangst;
    }

    public void setTemperatuurrangeBijOntvangst(Integer temperatuurrangeBijOntvangst) {
        this.temperatuurrangeBijOntvangst = temperatuurrangeBijOntvangst;
    }

    public String getBewaarinstructies() {
        return bewaarinstructies;
    }

    public void setBewaarinstructies(String bewaarinstructies) {
        this.bewaarinstructies = bewaarinstructies;
    }

    public String getBereidingsWijze2500() {
        return bereidingsWijze2500;
    }

    public void setBereidingsWijze2500(String bereidingsWijze2500) {
        this.bereidingsWijze2500 = bereidingsWijze2500;
    }

    public String getGs1plac0024() {
        return gs1plac0024;
    }

    public void setGs1plac0024(String gs1plac0024) {
        this.gs1plac0024 = gs1plac0024;
    }

    public String getArtikelStatus() {
        return artikelStatus;
    }

    public void setArtikelStatus(String artikelStatus) {
        this.artikelStatus = artikelStatus;
    }

    public String getVangstGebied() {
        return vangstGebied;
    }

    public void setVangstGebied(String vangstGebied) {
        this.vangstGebied = vangstGebied;
    }

    public String getStatGoederennrImportcodenr() {
        return statGoederennrImportcodenr;
    }

    public void setStatGoederennrImportcodenr(String statGoederennrImportcodenr) {
        this.statGoederennrImportcodenr = statGoederennrImportcodenr;
    }

    public Integer getBewaarAdviesMinimaleTemperatuur() {
        switch(temperatuurrangeBijOntvangst){
            case 1 :
                return 1;
            default:
                return 0;
        }
    }

    public Integer getBewaarAdviesMaximaleTemperatuur() {
        switch(temperatuurrangeBijOntvangst){
                case 1 :
                    return -18;
                case 2 :
                    return 7;
                case 3 :
                    return 25;
                case 4 :
                case 5 :
                case 6 :
                    return 4;
                default:
                    return 0;
            }
    }

    public String getHanosIndicator() {
        return hanosIndicator;
    }

    public void setHanosIndicator(String hanosIndicator) {
        this.hanosIndicator = hanosIndicator;
    }

    public List<String> getKeurmerkenLijst() {
        return keurmerkenLijst;
    }

    public void addKeurmerk(String keurmerkID) {
        if(this.keurmerkenLijst == null) {
            this.keurmerkenLijst = new ArrayList<>();
        }
        keurmerkenLijst.add(keurmerkID);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", besteleenheidId=" + besteleenheidId +
                ", inkoopInfoRecordId=" + inkoopInfoRecordId +
                ", voedingswaardeVerzameling=" + voedingswaardeVerzameling +
                ", merkId='" + merkId + '\'' +
                ", allergeenVerzameling=" + allergeenVerzameling +
                ", verpakkingVanHetProductArtikel='" + verpakkingVanHetProductArtikel + '\'' +
                ", thtTermijnInDagen=" + thtTermijnInDagen +
                ", thtTermijnWinkelInDagen=" + thtTermijnWinkelInDagen +
                ", InhoudVanHetProduct=" + inhoudVanHetProduct +
                ", EenheidVanHetProduct='" + eenheidVanHetProduct + '\'' +
                ", AlcoholPercentage=" + alcoholPercentage +
                ", Ingredienten='" + ingredienten + '\'' +
                ", TemperatuurrangeBijOntvangst=" + temperatuurrangeBijOntvangst +
                ", Bewaarinstructies='" + bewaarinstructies + '\'' +
                ", BereidingsWijze2500='" + bereidingsWijze2500 + '\'' +
                ", GS1plac0024='" + gs1plac0024 + '\'' +
                ", ArtikelStatus='" + artikelStatus + '\'' +
                ", vangstGebied='" + vangstGebied + '\'' +
                ", statGoederennrImportcodenr='" + statGoederennrImportcodenr + '\'' +
                ", hanosIndicator='" + hanosIndicator + '\'' +
                '}';
    }
}
