package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor Overige informatie over een artikel
public class Overig {
private String productNaam = "";
private String hanosIndicator;
private String artikelStatusProduct;
private String artikelStatusCE;
private String artikelStatusPE;
private String ceNummer;

public Overig(String productNaam, String hanosIndicator, String artikelStatusProduct, String artikelStatusCE, String artikelStatusPE,
		String ceNummer) {
   this.productNaam = productNaam;
   this.hanosIndicator = hanosIndicator;
   this.artikelStatusProduct = artikelStatusProduct;
   this.artikelStatusCE = artikelStatusCE;
   this.artikelStatusPE = artikelStatusPE;
   this.ceNummer = ceNummer;
}

public String getHanosIndicator() {
	return hanosIndicator;
}

public void setHanosIndicator(String hanosIndicator) {
	this.hanosIndicator = hanosIndicator;
}

public String getArtikelStatusProduct() {
	return artikelStatusProduct;
}

public void setArtikelStatusProduct(String artikelStatusProduct) {
	this.artikelStatusProduct = artikelStatusProduct;
}

public String getArtikelStatusCE() {
	return artikelStatusCE;
}

public void setArtikelStatusCE(String artikelStatusCE) {
	this.artikelStatusCE = artikelStatusCE;
}

public String getArtikelStatusPE() {
	return artikelStatusPE;
}

public void setArtikelStatusPE(String artikelStatusPE) {
	this.artikelStatusPE = artikelStatusPE;
}

public String getProductNaam() {
   return productNaam;
}

public void setProductNaam(String productNaam) {
   this.productNaam = productNaam;
}

public String getCeNummer() {
	return ceNummer;
}

public void setCeNummer(String ceNummer) {
	this.ceNummer = ceNummer;
}

@Override
public String toString() {
   return "Keurmerk{" +
  		 "ProductNaam='" + productNaam + "'" +
           "hanosIndicator='" + hanosIndicator + "'" +
           ", artikelStatusProduct='" + artikelStatusProduct + "'" +
           ", artikelStatusCE='" + artikelStatusCE + "'" +
           ", artikelStatusPE='" + artikelStatusPE + "'" +
           ", ceNummer='" + ceNummer + "'" +
           "}";
}
}
