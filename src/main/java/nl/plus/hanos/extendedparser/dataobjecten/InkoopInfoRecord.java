package nl.plus.hanos.extendedparser.dataobjecten;

public class InkoopInfoRecord {
    private String id = "";

    public InkoopInfoRecord() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "InkoopInfoRecord{" +
                "id='" + id + '\'' +
                '}';
    }
}
