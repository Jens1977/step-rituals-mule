package nl.plus.hanos.extendedparser.dataobjecten;

public class ConsumentenEenheid {
    private String consumentenEenheidId = "";
    private Double ceBrutoGewicht;
    private Double ceNettoGewicht;
    private Double ceLengte;
    private Double ceBreedte;
    private Double ceHoogte;
    private String ceArtikelStatus = "";
    private String ceNummer;
    private String eanCodeCE;
    private String artikelVariantID = "";
    private String productID = "";

    public ConsumentenEenheid(String id) {
        this.consumentenEenheidId = id;
    }

    public String getConsumentenEenheidId() {
        return consumentenEenheidId;
    }

    public void setConsumentenEenheidId(String consumentenEenheidId) {
        this.consumentenEenheidId = consumentenEenheidId;
    }

    public Double getCeBrutoGewicht() {
        return ceBrutoGewicht;
    }

    public void setCeBrutoGewicht(Double ceBrutoGewicht) {
        this.ceBrutoGewicht = ceBrutoGewicht;
    }

    public Double getCeNettoGewicht() {
        return ceNettoGewicht;
    }

    public void setCeNettoGewicht(Double ceNettoGewicht) {
        this.ceNettoGewicht = ceNettoGewicht;
    }

    public Double getCeLengte() {
        return ceLengte;
    }

    public void setCeLengte(Double ceLengte) {
        this.ceLengte = ceLengte;
    }

    public Double getCeBreedte() {
        return ceBreedte;
    }

    public void setCeBreedte(Double ceBreedte) {
        this.ceBreedte = ceBreedte;
    }

    public Double getCeHoogte() {
        return ceHoogte;
    }

    public void setCeHoogte(Double ceHoogte) {
        this.ceHoogte = ceHoogte;
    }

    public String getCeArtikelStatus() {
        return ceArtikelStatus;
    }

    public void setCeArtikelStatus(String ceArtikelStatus) {
        this.ceArtikelStatus = ceArtikelStatus;
    }

    public String getCeNummer() {
        return ceNummer;
    }

    public void setCeNummer(String ceNummer) {
        this.ceNummer = ceNummer;
    }

    public String getEanCodeCE() {
        return eanCodeCE;
    }

    public void setEanCodeCE(String eanCodeCE) {
        this.eanCodeCE = eanCodeCE;
    }

    public String getArtikelVariantID() {
        return artikelVariantID;
    }

    public void setArtikelVariantID(String artikelVariantID) {
        this.artikelVariantID = artikelVariantID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    @Override
    public String toString() {
        return "ConsumentenEenheid{" +
                "consumentenEenheidId='" + consumentenEenheidId + '\'' +
                ", ceBrutoGewicht=" + ceBrutoGewicht +
                ", ceNettoGewicht=" + ceNettoGewicht +
                ", ceLengte=" + ceLengte +
                ", ceBreedte=" + ceBreedte +
                ", ceHoogte=" + ceHoogte +
                ", ceArtikelStatus='" + ceArtikelStatus + '\'' +
                ", ceNummer=" + ceNummer +
                ", eanCodeCE='" + eanCodeCE + '\'' +
                ", artikelVariantID='" + artikelVariantID + '\'' +
                ", productID='" + productID + '\'' +
                '}';
    }
}
