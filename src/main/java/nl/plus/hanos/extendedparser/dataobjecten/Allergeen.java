package nl.plus.hanos.extendedparser.dataobjecten;

public class Allergeen {
    private String id;
    private String name;
    private String allergeenOmschrijving;
    private String allergeenCode;

    public Allergeen() {}

    public Allergeen(String id) {
        this.id = id;
    }

    public Allergeen(String id, String allergeenOmschrijving, String allergeenCode) {
        this.id = id;
        this.allergeenOmschrijving = allergeenOmschrijving;
        this.allergeenCode = allergeenCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAllergeenOmschrijving() {
        return allergeenOmschrijving;
    }

    public void setAllergeenOmschrijving(String allergeenOmschrijving) {
        this.allergeenOmschrijving = allergeenOmschrijving;
    }

    public String getAllergeenCode() {
        return allergeenCode;
    }

    public void setAllergeenCode(String allergeenCode) {
        this.allergeenCode = allergeenCode;
    }

    @Override
    public String toString() {
        return "Allergeen{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", AllergeenOmschrijving='" + allergeenOmschrijving + '\'' +
                ", AllergeenCode='" + allergeenCode + '\'' +
                '}';
    }
}
