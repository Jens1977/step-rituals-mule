package nl.plus.hanos.extendedparser.dataobjecten;

import java.util.ArrayList;
import java.util.List;

public class BestelEenheid {

    private String bestelEenheidId;
    private List<String> consumentenEenheidIDs = new ArrayList<>();
    private Integer aantalCEPerHE;
    private Double heBrutoGewichtInGram;
    private Double heLengteInMm;
    private Double heBreedteInMm;
    private Double heHoogteInMm;
    private Integer minimaleBestelhoeveelheidInCE;
    private String artikelStatusPE;
    private String eanCodePE;

    public BestelEenheid() {}

    public BestelEenheid(String id) {
        this.bestelEenheidId = id;
    }

    public String getBestelEenheidId() {
        return bestelEenheidId;
    }

    public List<String> getConsumentenEenheidIDs() {
        return consumentenEenheidIDs;
    }

    public void setConsumentenEenheidIDs(List<String> consumentenEenheidIDs) {
        this.consumentenEenheidIDs = consumentenEenheidIDs;
    }

    public void addConsumentenEenheidID(String consumentenEenheidID) {
        if (consumentenEenheidIDs == null) {
            consumentenEenheidIDs = new ArrayList<>();
        }
        consumentenEenheidIDs.add(consumentenEenheidID);
    }

    public void setBestelEenheidId(String bestelEenheidId) {
        this.bestelEenheidId = bestelEenheidId;
    }

    public Integer getAantalCEPerHE() {
        return aantalCEPerHE;
    }

    public void setAantalCEPerHE(Integer aantalCEPerHE) {
        this.aantalCEPerHE = aantalCEPerHE;
    }

    public Double getHeBrutoGewichtInGram() {
        return heBrutoGewichtInGram;
    }

    public void setHeBrutoGewichtInGram(Double heBrutoGewichtInGram) {
        this.heBrutoGewichtInGram = heBrutoGewichtInGram;
    }

    public Double getHeLengteInMm() {
        return heLengteInMm;
    }

    public void setHeLengteInMm(Double heLengteInMm) {
        this.heLengteInMm = heLengteInMm;
    }

    public Double getHeBreedteInMm() {
        return heBreedteInMm;
    }

    public void setHeBreedteInMm(Double heBreedteInMm) {
        this.heBreedteInMm = heBreedteInMm;
    }

    public Double getHeHoogteInMm() {
        return heHoogteInMm;
    }

    public void setHeHoogteInMm(Double heHoogteInMm) {
        this.heHoogteInMm = heHoogteInMm;
    }

    public Integer getMinimaleBestelhoeveelheidInCE() {
        return minimaleBestelhoeveelheidInCE;
    }

    public void setMinimaleBestelhoeveelheidInCE(Integer minimaleBestelhoeveelheidInCE) {
        this.minimaleBestelhoeveelheidInCE = minimaleBestelhoeveelheidInCE;
    }

    public String getArtikelStatusPE() {
        return artikelStatusPE;
    }

    public void setArtikelStatusPE(String artikelStatusPE) {
        this.artikelStatusPE = artikelStatusPE;
    }

    public String getEanCodePE() {
        return eanCodePE;
    }

    public void setEanCodePE(String eanCodePE) {
        this.eanCodePE = eanCodePE;
    }

    @Override
    public String toString() {
        return "BestelEenheid{" +
                "bestelEenheidId='" + bestelEenheidId + '\'' +
                ", consumentenEenheidIDs=" + consumentenEenheidIDs +
                ", aantalCEPerHE=" + aantalCEPerHE +
                ", heBrutoGewichtInGram=" + heBrutoGewichtInGram +
                ", heLengteInMm=" + heLengteInMm +
                ", heBreedteInMm=" + heBreedteInMm +
                ", heHoogteInMm=" + heHoogteInMm +
                ", minimaleBestelhoeveelheidInCE=" + minimaleBestelhoeveelheidInCE +
                ", artikelStatusPE='" + artikelStatusPE + '\'' +
                ", eanCodePE='" + eanCodePE + '\'' +
                '}';
    }
}
