package nl.plus.hanos.extendedparser.dataobjecten;

public class Merk {
    private String id;
    private String name;
    private String afdrukindicator;
    private String marketingOmschrijving20;
    private String merkClassificatieCode;
    private String omschrijving20;
    private String merkcode;

    public Merk() {}

    public Merk(String id) {
        this.id = id;
    }

    public Merk(String id, String omschrijving) {
        this.id = id;
        this.omschrijving20 = omschrijving;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAfdrukindicator() {
        return afdrukindicator;
    }

    public void setAfdrukindicator(String afdrukindicator) {
        this.afdrukindicator = afdrukindicator;
    }

    public String getMarketingOmschrijving20() {
        return marketingOmschrijving20;
    }

    public void setMarketingOmschrijving20(String marketingOmschrijving20) {
        this.marketingOmschrijving20 = marketingOmschrijving20;
    }

    public String getMerkClassificatieCode() {
        return merkClassificatieCode;
    }

    public void setMerkClassificatieCode(String merkClassificatieCode) {
        this.merkClassificatieCode = merkClassificatieCode;
    }

    public String getOmschrijving20() {
        return omschrijving20;
    }

    public void setOmschrijving20(String omschrijving20) {
        this.omschrijving20 = omschrijving20;
    }

    public String getMerkcode() {
        return merkcode;
    }

    public void setMerkcode(String merkcode) {
        this.merkcode = merkcode;
    }

    @Override
    public String toString() {
        return "Merk{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", afdrukindicator='" + afdrukindicator + '\'' +
                ", MarketingOmschrijving20='" + marketingOmschrijving20 + '\'' +
                ", MerkClassificatieCode='" + merkClassificatieCode + '\'' +
                ", Omschrijving20='" + omschrijving20 + '\'' +
                ", Merkcode='" + merkcode + '\'' +
                '}';
    }
}
