package nl.plus.hanos.extendedparser.dataobjecten;

public class VoedingswaardeEntity {
    private String id;
    private String name;
    private String voedingswaardeOmschrijving;
    private String voedingswaardeCode;
    private String voedingswaardeCodeGS1;
    private String sortVoedingswaarden;

    public VoedingswaardeEntity() {}

    public VoedingswaardeEntity(String id, String code) {
        this.id = id;
        this.voedingswaardeCode = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoedingswaardeOmschrijving() {
        return voedingswaardeOmschrijving;
    }

    public void setVoedingswaardeOmschrijving(String voedingswaardeOmschrijving) {
        this.voedingswaardeOmschrijving = voedingswaardeOmschrijving;
    }

    public String getVoedingswaardeCode() {
        return voedingswaardeCode;
    }

    public void setVoedingswaardeCode(String voedingswaardeCode) {
        this.voedingswaardeCode = voedingswaardeCode;
    }

    public String getVoedingswaardeCodeGS1() {
        return voedingswaardeCodeGS1;
    }

    public void setVoedingswaardeCodeGS1(String voedingswaardeCodeGS1) {
        this.voedingswaardeCodeGS1 = voedingswaardeCodeGS1;
    }

    public String getSortVoedingswaarden() {
        return sortVoedingswaarden;
    }

    public void setSortVoedingswaarden(String sortVoedingswaarden) {
        this.sortVoedingswaarden = sortVoedingswaarden;
    }

    @Override
    public String toString() {
        return "Voedingswaarde{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", VoedingswaardeOmschrijving='" + voedingswaardeOmschrijving + '\'' +
                ", VoedingswaardeCode='" + voedingswaardeCode + '\'' +
                ", VoedingswaardeCodeGS1='" + voedingswaardeCodeGS1 + '\'' +
                ", SortVoedingswaarden='" + sortVoedingswaarden + '\'' +
                '}';
    }
}
