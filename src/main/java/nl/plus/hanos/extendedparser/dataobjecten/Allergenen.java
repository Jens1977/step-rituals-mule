package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor allergenen informatie over een artikel
public class Allergenen {
 private String productNaam = "";
 private String gluten;
 private String schaaldieren;
 private String ei;
 private String vis;
 private String aardnoten;
 private String soja;
 private String melk;
 private String noten;
 private String selderij;
 private String mosterd;
 private String sesam;
 private String zwafelEnSulfieten;
 private String lupine;
 private String weekdieren;

 public Allergenen(String productNaam, String gluten, String schaaldieren, String ei, String vis, String aardnoten,
                   String soja, String melk, String noten, String selderij, String mosterd, String sesam,
                   String zwafelEnSulfieten, String lupine, String weekdieren) {
     this.productNaam = productNaam;
     this.gluten = gluten;
     this.schaaldieren = schaaldieren;
     this.ei = ei;
     this.vis = vis;
     this.aardnoten = aardnoten;
     this.soja = soja;
     this.melk = melk;
     this.noten = noten;
     this.selderij = selderij;
     this.mosterd = mosterd;
     this.sesam = sesam;
     this.zwafelEnSulfieten = zwafelEnSulfieten;
     this.lupine = lupine;
     this.weekdieren = weekdieren;
 }

 public String getProductNaam() {
     return productNaam;
 }

 public void setProductNaam(String productNaam) {
     this.productNaam = productNaam;
 }

 public String getGluten() {
     return gluten;
 }

 public void setGluten(String gluten) {
     this.gluten = gluten;
 }

 public String getSchaaldieren() {
     return schaaldieren;
 }

 public void setSchaaldieren(String schaaldieren) {
     this.schaaldieren = schaaldieren;
 }

 public String getEi() {
     return ei;
 }

 public void setEi(String ei) {
     this.ei = ei;
 }

 public String getVis() {
     return vis;
 }

 public void setVis(String vis) {
     this.vis = vis;
 }

 public String getAardnoten() {
     return aardnoten;
 }

 public void setAardnoten(String aardnoten) {
     this.aardnoten = aardnoten;
 }

 public String getSoja() {
     return soja;
 }

 public void setSoja(String soja) {
     this.soja = soja;
 }

 public String getMelk() {
     return melk;
 }

 public void setMelk(String melk) {
     this.melk = melk;
 }

 public String getNoten() {
     return noten;
 }

 public void setNoten(String noten) {
     this.noten = noten;
 }

 public String getSelderij() {
     return selderij;
 }

 public void setSelderij(String selderij) {
     this.selderij = selderij;
 }

 public String getMosterd() {
     return mosterd;
 }

 public void setMosterd(String mosterd) {
     this.mosterd = mosterd;
 }

 public String getSesam() {
     return sesam;
 }

 public void setSesam(String sesam) {
     this.sesam = sesam;
 }

 public String getZwafelEnSulfieten() {
     return zwafelEnSulfieten;
 }

 public void setZwafelEnSulfieten(String zwafelEnSulfieten) {
     this.zwafelEnSulfieten = zwafelEnSulfieten;
 }

 public String getLupine() {
     return lupine;
 }

 public void setLupine(String lupine) {
     this.lupine = lupine;
 }

 public String getWeekdieren() {
     return weekdieren;
 }

 public void setWeekdieren(String weekdieren) {
     this.weekdieren = weekdieren;
 }

 @Override
 public String toString() {
     return "Allergenen{" +
    		 "productNaam='" + productNaam + "'" +
             ", gluten='" + gluten + "'" +
             ", schaaldieren='" + schaaldieren + "'" +
             ", ei='" + ei + "'" +
             ", vis='" + vis + "'" +
             ", aardnoten='" + aardnoten + "'" +
             ", soja='" + soja + "'" +
             ", melk='" + melk + "'" +
             ", noten='" + noten + "'" +
             ", selderij='" + selderij + "'" +
             ", mosterd='" + mosterd + "'" +
             ", sesam='" + sesam + "'" +
             ", zwafelEnSulfieten='" + zwafelEnSulfieten + "'" +
             ", lupine='" + lupine + "'" +
             ", weekdieren='" + weekdieren + "'" +
             "}";
 }
}
