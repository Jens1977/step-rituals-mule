package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor product specifieke informatie over een artikel
public class ProductSpecifiek {
 private String productNaam = "";
 private Double alcoholPercentage;
 private String plato;
 private String accijnscategorie;
 private String latijnseNaam;
 private String vangstgebied;
 private String vangstmethode;
 private String vetpercentageInDrogeStof;
 private String gebruikteMelk;
 private String verpaktOnderBeschermendeAtmosfeer;
 private String bestraald;
 private String hoogCafeineGehalte;
 private String zoetstoffen;
 private String toegevoegdWater;
 private String fytosterolen;

 public ProductSpecifiek(String productNaam, Double alcoholPercentage, String plato, String accijnscategorie,
                         String latijnseNaam, String vangstgebied, String vangstmethode,
                         String vetpercentageInDrogeStof, String gebruikteMelk,
                         String verpaktOnderBeschermendeAtmosfeer, String bestraald, String hoogCafeineGehalte,
                         String zoetstoffen, String toegevoegdWater, String fytosterolen) {
     this.productNaam = productNaam;
     this.alcoholPercentage = alcoholPercentage;
     this.plato = plato;
     this.accijnscategorie = accijnscategorie;
     this.latijnseNaam = latijnseNaam;
     this.vangstgebied = vangstgebied;
     this.vangstmethode = vangstmethode;
     this.vetpercentageInDrogeStof = vetpercentageInDrogeStof;
     this.gebruikteMelk = gebruikteMelk;
     this.verpaktOnderBeschermendeAtmosfeer = verpaktOnderBeschermendeAtmosfeer;
     this.bestraald = bestraald;
     this.hoogCafeineGehalte = hoogCafeineGehalte;
     this.zoetstoffen = zoetstoffen;
     this.toegevoegdWater = toegevoegdWater;
     this.fytosterolen = fytosterolen;
 }

 public String getProductNaam() {
     return productNaam;
 }

 public void setProductNaam(String productNaam) {
     this.productNaam = productNaam;
 }

 public Double getAlcoholPercentage() {
     return alcoholPercentage;
 }

 public void setAlcoholPercentage(Double alcoholPercentage) {
     this.alcoholPercentage = alcoholPercentage;
 }

 public String getPlato() {
     return plato;
 }

 public void setPlato(String plato) {
     this.plato = plato;
 }

 public String getAccijnscategorie() {
     return accijnscategorie;
 }

 public void setAccijnscategorie(String accijnscategorie) {
     this.accijnscategorie = accijnscategorie;
 }

 public String getLatijnseNaam() {
     return latijnseNaam;
 }

 public void setLatijnseNaam(String latijnseNaam) {
     this.latijnseNaam = latijnseNaam;
 }

 public String getVangstgebied() {
     return vangstgebied;
 }

 public void setVangstgebied(String vangstgebied) {
     this.vangstgebied = vangstgebied;
 }

 public String getVangstmethode() {
     return vangstmethode;
 }

 public void setVangstmethode(String vangstmethode) {
     this.vangstmethode = vangstmethode;
 }

 public String getVetpercentageInDrogeStof() {
     return vetpercentageInDrogeStof;
 }

 public void setVetpercentageInDrogeStof(String vetpercentageInDrogeStof) {
     this.vetpercentageInDrogeStof = vetpercentageInDrogeStof;
 }

 public String getGebruikteMelk() {
     return gebruikteMelk;
 }

 public void setGebruikteMelk(String gebruikteMelk) {
     this.gebruikteMelk = gebruikteMelk;
 }

 public String getVerpaktOnderBeschermendeAtmosfeer() {
     return verpaktOnderBeschermendeAtmosfeer;
 }

 public void setVerpaktOnderBeschermendeAtmosfeer(String verpaktOnderBeschermendeAtmosfeer) {
     this.verpaktOnderBeschermendeAtmosfeer = verpaktOnderBeschermendeAtmosfeer;
 }

 public String getBestraald() {
     return bestraald;
 }

 public void setBestraald(String bestraald) {
     this.bestraald = bestraald;
 }

 public String getHoogCafeineGehalte() {
     return hoogCafeineGehalte;
 }

 public void setHoogCafeineGehalte(String hoogCafeineGehalte) {
     this.hoogCafeineGehalte = hoogCafeineGehalte;
 }

 public String getZoetstoffen() {
     return zoetstoffen;
 }

 public void setZoetstoffen(String zoetstoffen) {
     this.zoetstoffen = zoetstoffen;
 }

 public String getToegevoegdWater() {
     return toegevoegdWater;
 }

 public void setToegevoegdWater(String toegevoegdWater) {
     this.toegevoegdWater = toegevoegdWater;
 }

 public String getFytosterolen() {
     return fytosterolen;
 }

 public void setFytosterolen(String fytosterolen) {
     this.fytosterolen = fytosterolen;
 }

 @Override
 public String toString() {
     return "ProductSpecifiek{" +
    		 "ProductNaam='" + productNaam + "'" +
             ", alcoholPercentage='" + alcoholPercentage + "'" +
             ", plato='" + plato + "'" +
             ", accijnscategorie='" + accijnscategorie + "'" +
             ", latijnseNaam='" + latijnseNaam + "'" +
             ", vangstgebied='" + vangstgebied + "'" +
             ", vangstmethode='" + vangstmethode + "'" +
             ", vetpercentageInDrogeStof='" + vetpercentageInDrogeStof + "'" +
             ", gebruikteMelk='" + gebruikteMelk + "'" +
             ", verpaktOnderBeschermendeAtmosfeer='" + verpaktOnderBeschermendeAtmosfeer + "'" +
             ", bestraald='" + bestraald + "'" +
             ", hoogCafeineGehalte='" + hoogCafeineGehalte + "'" +
             ", zoetstoffen='" + zoetstoffen + "'" +
             ", toegevoegdWater='" + toegevoegdWater + "'" +
             ", fytosterolen='" + fytosterolen + "'" +
             "}";
 }
}
