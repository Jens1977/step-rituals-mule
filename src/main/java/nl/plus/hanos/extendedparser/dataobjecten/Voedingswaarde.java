package nl.plus.hanos.extendedparser.dataobjecten;

public class Voedingswaarde {
    private String id = null;
    private Double voedingswaarde = 0.0;
    private String eenheid = null;
    private int sortVoedingswaardenCalc = 0;

    public Voedingswaarde() {}

    public Voedingswaarde(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getVoedingswaarde() {
        return voedingswaarde;
    }

    public void setVoedingswaarde(double voedingswaarde) {
        this.voedingswaarde = voedingswaarde;
    }

    public String getEenheid() {
        return eenheid;
    }

    public void setEenheid(String eenheid) {
        this.eenheid = eenheid;
    }

    public int getSortVoedingswaardenCalc() {
        return sortVoedingswaardenCalc;
    }

    public void setSortVoedingswaardenCalc(int sortVoedingswaardenCalc) {
        this.sortVoedingswaardenCalc = sortVoedingswaardenCalc;
    }

    @Override
    public String toString() {
        return "Voedingswaarde{" +
                "entityId='" + id + '\'' +
                ", voedingswaarde=" + voedingswaarde +
                ", eenheid='" + eenheid + '\'' +
                ", sortVoedingswaardenCalc=" + sortVoedingswaardenCalc +
                '}';
    }
}
