package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor algemene informatie over een artikel
public class Algemeen {
 private String artikelnummerLeverancier;
 private String eanCodeCE;
 private String datapool;
 private String artikelomschrijvingNederlands;
 private String omschrijvingDuits;
 private String omschrijvingEngels;
 private String omschrijvingFrans;
 private String verpakking;
 private Integer inhoud;
 private String eenheid;
 private String landVanOorsprong;
 private String statGoederennrImportcodenr;
 private String merk;

 public Algemeen(String artikelnummerLeverancier,
                 String eanCodeCE, String datapool,
                 String artikelomschrijvingNederlands,
                 String omschrijvingDuits, String omschrijvingEngels,
                 String omschrijvingFrans, String verpakking,
                 Integer inhoud, String eenheid, String landVanOorsprong,
                 String statGoederennrImportcodenr, String merk) {
     this.artikelnummerLeverancier = artikelnummerLeverancier;
     this.eanCodeCE = eanCodeCE;
     this.datapool = datapool;
     this.artikelomschrijvingNederlands = artikelomschrijvingNederlands;
     this.omschrijvingDuits = omschrijvingDuits;
     this.omschrijvingEngels = omschrijvingEngels;
     this.omschrijvingFrans = omschrijvingFrans;
     this.verpakking = verpakking;
     this.inhoud = inhoud;
     this.eenheid = eenheid;
     this.landVanOorsprong = landVanOorsprong;
     this.statGoederennrImportcodenr = statGoederennrImportcodenr;
     this.merk = merk;
 }

 public String getArtikelnummerLeverancier() {
     return artikelnummerLeverancier;
 }

 public void setArtikelnummerLeverancier(String artikelnummerLeverancier) {
     this.artikelnummerLeverancier = artikelnummerLeverancier;
 }

 public String getEanCodeCE() {
     return eanCodeCE;
 }

 public void setEanCodeCE(String eanCodeCE) {
     this.eanCodeCE = eanCodeCE;
 }

 public String getDatapool() {
     return datapool;
 }

 public void setDatapool(String datapool) {
     this.datapool = datapool;
 }

 public String getArtikelomschrijvingNederlands() {
     return artikelomschrijvingNederlands;
 }

 public void setArtikelomschrijvingNederlands(String artikelomschrijvingNederlands) {
     this.artikelomschrijvingNederlands = artikelomschrijvingNederlands;
 }

 public String getOmschrijvingDuits() {
     return omschrijvingDuits;
 }

 public void setOmschrijvingDuits(String omschrijvingDuits) {
     this.omschrijvingDuits = omschrijvingDuits;
 }

 public String getOmschrijvingEngels() {
     return omschrijvingEngels;
 }

 public void setOmschrijvingEngels(String omschrijvingEngels) {
     this.omschrijvingEngels = omschrijvingEngels;
 }

 public String getOmschrijvingFrans() {
     return omschrijvingFrans;
 }

 public void setOmschrijvingFrans(String omschrijvingFrans) {
     this.omschrijvingFrans = omschrijvingFrans;
 }

 public String getVerpakking() {
     return verpakking;
 }

 public void setVerpakking(String verpakking) {
     this.verpakking = verpakking;
 }

 public Integer getInhoud() {
     return inhoud;
 }

 public void setInhoud(Integer inhoud) {
     this.inhoud = inhoud;
 }

 public String getEenheid() {
     return eenheid;
 }

 public void setEenheid(String eenheid) {
     this.eenheid = eenheid;
 }

 public String getLandVanOorsprong() {
     return landVanOorsprong;
 }

 public void setLandVanOorsprong(String landVanOorsprong) {
     this.landVanOorsprong = landVanOorsprong;
 }

 public String getStatGoederennrImportcodenr() {
     return statGoederennrImportcodenr;
 }

 public void setStatGoederennrImportcodenr(String statGoederennrImportcodenr) {
     this.statGoederennrImportcodenr = statGoederennrImportcodenr;
 }

 public String getMerk() {
     return merk;
 }

 public void setMerk(String merk) {
     this.merk = merk;
 }

 @Override
 public String toString() {
     return "Algemeen{" +
             "artikelnummerLeverancier='" + artikelnummerLeverancier + "'" +
             ", eanCodeCE='" + eanCodeCE + "'" +
             ", datapool='" + datapool + "'" +
             ", artikelomschrijvingNederlands='" + artikelomschrijvingNederlands + "'" +
             ", omschrijvingDuits='" + omschrijvingDuits + "'" +
             ", omschrijvingEngels='" + omschrijvingEngels + "'" +
             ", omschrijvingFrans='" + omschrijvingFrans + "'" +
             ", verpakking='" + verpakking + "'" +
             ", inhoud='" + inhoud + "'" +
             ", eenheid='" + eenheid + "'" +
             ", landVanOorsprong='" + landVanOorsprong + "'" +
             ", statGoederennrImportcodenr='" + statGoederennrImportcodenr + "'" +
             ", merk='" + merk + "'" +
             "}";
 }
}

