package nl.plus.hanos.extendedparser.dataobjecten;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class VoedingswaardeList {
    private Map<String, Voedingswaarde> voedingswaardeCollectie = new HashMap<>();

    public VoedingswaardeList() {
    }

    public Double getVoedingswaardeValueByKey(String voedingswaardeKey) {

        if (voedingswaardeCollectie.containsKey(voedingswaardeKey)) {
            return voedingswaardeCollectie.get(voedingswaardeKey).getVoedingswaarde();
        }
        return null;
    }

    public void addVoedingswaarde(Voedingswaarde voedingswaarde) {
        voedingswaardeCollectie.put(voedingswaarde.getId(), voedingswaarde);
    }

    @Override
    public String toString() {
        StringBuffer stringDing = new StringBuffer();
        Set<String> keySet = voedingswaardeCollectie.keySet();
        Iterator<String> keyIterator = keySet.iterator();
        stringDing.append("Aanwezige voedingswaarden {\n");
        while(keyIterator.hasNext()){
            String key = keyIterator.next();
            stringDing.append(voedingswaardeCollectie.get(key).getId());
            stringDing.append(" - ");
            stringDing.append(voedingswaardeCollectie.get(key).getVoedingswaarde());
            stringDing.append("\n");
        }
        stringDing.append("}");

        return stringDing.toString();
    }
}
