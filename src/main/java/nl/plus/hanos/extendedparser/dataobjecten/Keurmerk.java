package nl.plus.hanos.extendedparser.dataobjecten;

public class Keurmerk {
    private String id = "";
    private String logoCode = "";

    public Keurmerk(String id) {
        this.id = id;
    }

    public Keurmerk(String id, String logoCode) {
        this.id = id;
        setLogoCode(logoCode);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogoCode() {
        return logoCode;
    }

    public void setLogoCode(String logoCode) {

        switch(logoCode) {
            case "AQUACULTURE_STEWARDSHIP_COUNCIL" :
                this.logoCode = "ASC Duurzaam gekweekte vis";
                break;
            case "BETER_LEVEN_1_STER | BETER_LEVEN_2_STER | BETER_LEVEN_3_STER":
                this.logoCode = "Beter Leven";
                break;
            case "BETER_LEVEN_1_STER" :
                this.logoCode = "Beter Leven 1 ster";
                break;
            case "BETER_LEVEN_2_STER":
                this.logoCode = "Beter Leven 2 sterren";
                break;
            case "EU_ORGANIC_FARMING | EKO" :
                this.logoCode = "Biologisch / Eko";
                break;
            case "FAIR_TRADE_MARK | FAIRTRADE_COCOA | FAIRTRADE_SUGAR":
                this.logoCode = "Fair Trade";
                break;
            case "FREE_FROM_GLUTEN" :
                this.logoCode = "Glutenvrij";
                break;
            case "HALAL":
                this.logoCode = "Halal";
                break;
            case "KOSHER" :
                this.logoCode = "Kosher";
                break;
            case "FREE_FROM_LACTOSE":
                this.logoCode = "Lactose Vrij";
                break;
            case "MARINE_STEWARDSHIP_COUNCIL_LABEL" :
                this.logoCode = "MSC Duurzaam gevangen vis";
                break;
            case "RAINFOREST_ALLIANCE":
                this.logoCode = "Rain Forest Alliance";
                break;
            case "UTZ_CERTIFIED" :
                this.logoCode = "UTZ";
                break;
            case "EUROPEAN_V_LABEL_VEGAN":
                this.logoCode = "Veganistisch";
                break;
            case "VEGETARIAN | EUROPEAN_V_LABEL_VEGETARIAN" :
                this.logoCode = "Vegetarisch";
                break;
            case "ON_THE_WAY_TO_PLANET_PROOF":
                this.logoCode = "On the way to planet proof";
                break;
            default :
                this.logoCode = "";
                break;
        }
    }

    @Override
    public String toString() {
        return "Keurmerk{" +
                "id='" + id + '\'' +
                ", logoCode='" + logoCode + '\'' +
                '}';
    }
}
