package nl.plus.hanos.extendedparser.dataobjecten;

public class ArtikelVariant {
    private String id;
    private String name;
    private String afdrukindicator;
    private String omschrijving40;
    private String artikelVariantCode ;
    private String statistiekIndicator;
    private String voorraadhoudendType;

    public ArtikelVariant() {}

    public ArtikelVariant(String id) {
        this.id = id;
    }
    public ArtikelVariant(String id, String artikelVariantCode) {
        this.id = id;
        this.artikelVariantCode = artikelVariantCode;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAfdrukindicator() {
        return afdrukindicator;
    }

    public void setAfdrukindicator(String afdrukindicator) {
        this.afdrukindicator = afdrukindicator;
    }

    public String getOmschrijving40() {
        return omschrijving40;
    }

    public void setOmschrijving40(String omschrijving40) {
        this.omschrijving40 = omschrijving40;
    }

    public String getArtikelVariantCode() {
        return artikelVariantCode;
    }

    public void setArtikelVariantCode(String artikelVariantCode) {
        this.artikelVariantCode = artikelVariantCode;
    }

    public String getStatistiekIndicator() {
        return statistiekIndicator;
    }

    public void setStatistiekIndicator(String statistiekIndicator) {
        this.statistiekIndicator = statistiekIndicator;
    }

    public String getVoorraadhoudendType() {
        return voorraadhoudendType;
    }

    public void setVoorraadhoudendType(String voorraadhoudendType) {
        this.voorraadhoudendType = voorraadhoudendType;
    }

    @Override
    public String toString() {
        return "ArtikelVariant{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", afdrukindicator='" + afdrukindicator + '\'' +
                ", omschrijving40='" + omschrijving40 + '\'' +
                ", artikelVariantCode='" + artikelVariantCode + '\'' +
                ", statistiekIndicator='" + statistiekIndicator + '\'' +
                ", vaarraadhoudendType='" + voorraadhoudendType + '\'' +
                '}';
    }
}
