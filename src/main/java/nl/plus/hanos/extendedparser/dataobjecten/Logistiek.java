package nl.plus.hanos.extendedparser.dataobjecten;

//Standaard POJO voor logistieke informatie over een artikel
public class Logistiek {
 private String productNaam;
 private Double ceBrutoGewichtInGram;
 private Double ceNettoGewichtInGram;
 private Double ceLengteInMm;
 private Double ceBreedteInMm;
 private Double ceHoogteInMm;
 private Integer aantalCEPerHE;
 private String heEANCode;
 private Double heBrutoGewichtInGram;
 private Double heLengteInMm;
 private Double heBreedteInMm;
 private Double heHoogteInMm;
 private Integer aantalCEPerLaag;
 private Integer aantalCEPerPallet;
 private Integer minimaleBestelhoeveelheidInCE;

 public Logistiek(String productNaam, Double ceBrutoGewichtInGram, Double ceNettoGewichtInGram, Double ceLengteInMm,
                  Double ceBreedteInMm, Double ceHoogteInMm, Integer aantalCEPerHE, String heEANCode,
                  Double heBrutoGewichtInGram, Double heLengteInMm, Double heBreedteInMm, Double heHoogteInMm,
                  Integer aantalCEPerLaag, Integer aantalCEPerPallet, Integer minimaleBestelhoeveelheidInCE) {
     this.productNaam = productNaam;
     this.ceBrutoGewichtInGram = ceBrutoGewichtInGram;
     this.ceNettoGewichtInGram = ceNettoGewichtInGram;
     this.ceLengteInMm = ceLengteInMm;
     this.ceBreedteInMm = ceBreedteInMm;
     this.ceHoogteInMm = ceHoogteInMm;
     this.aantalCEPerHE = aantalCEPerHE;
     this.heEANCode = heEANCode;
     this.heBrutoGewichtInGram = heBrutoGewichtInGram;
     this.heLengteInMm = heLengteInMm;
     this.heBreedteInMm = heBreedteInMm;
     this.heHoogteInMm = heHoogteInMm;
     this.aantalCEPerLaag = aantalCEPerLaag;
     this.aantalCEPerPallet = aantalCEPerPallet;
     this.minimaleBestelhoeveelheidInCE = minimaleBestelhoeveelheidInCE;
 }

 public String getProductNaam() {
     return productNaam;
 }

 public void setProductNaam(String productNaam) {
     this.productNaam = productNaam.trim();
 }

 public Double getCeBrutoGewichtInGram() {
     return ceBrutoGewichtInGram;
 }

 public void setCeBrutoGewichtInGram(Double ceBrutoGewichtInGram) {
     this.ceBrutoGewichtInGram = ceBrutoGewichtInGram;
 }

 public Double getCeNettoGewichtInGram() {
     return ceNettoGewichtInGram;
 }

 public void setCeNettoGewichtInGram(Double ceNettoGewichtInGram) {
     this.ceNettoGewichtInGram = ceNettoGewichtInGram;
 }

 public Double getCeLengteInMm() {
     return ceLengteInMm;
 }

 public void setCeLengteInMm(Double ceLengteInMm) {
     this.ceLengteInMm = ceLengteInMm;
 }

 public Double getCeBreedteInMm() {
     return ceBreedteInMm;
 }

 public void setCeBreedteInMm(Double ceBreedteInMm) {
     this.ceBreedteInMm = ceBreedteInMm;
 }

 public Double getCeHoogteInMm() {
     return ceHoogteInMm;
 }

 public void setCeHoogteInMm(Double ceHoogteInMm) {
     this.ceHoogteInMm = ceHoogteInMm;
 }

 public Integer getAantalCEPerHE() {
     return aantalCEPerHE;
 }

 public void setAantalCEPerHE(Integer aantalCEPerHE) {
     this.aantalCEPerHE = aantalCEPerHE;
 }

 public String getHeEANCode() {
     return heEANCode;
 }

 public void setHeEANCode(String heEANCode) {
     this.heEANCode = heEANCode;
 }

 public Double getHeBrutoGewichtInGram() {
     return heBrutoGewichtInGram;
 }

 public void setHeBrutoGewichtInGram(Double heBrutoGewichtInGram) {
     this.heBrutoGewichtInGram = heBrutoGewichtInGram;
 }

 public Double getHeLengteInMm() {
     return heLengteInMm;
 }

 public void setHeLengteInMm(Double heLengteInMm) {
     this.heLengteInMm = heLengteInMm;
 }

 public Double getHeBreedteInMm() {
     return heBreedteInMm;
 }

 public void setHeBreedteInMm(Double heBreedteInMm) {
     this.heBreedteInMm = heBreedteInMm;
 }

 public Double getHeHoogteInMm() {
     return heHoogteInMm;
 }

 public void setHeHoogteInMm(Double heHoogteInMm) {
     this.heHoogteInMm = heHoogteInMm;
 }

 public Integer getAantalCEPerLaag() {
     return aantalCEPerLaag;
 }

 public void setAantalCEPerLaag(Integer aantalCEPerLaag) {
     this.aantalCEPerLaag = aantalCEPerLaag;
 }

 public Integer getAantalCEPerPallet() {
     return aantalCEPerPallet;
 }

 public void setAantalCEPerPallet(Integer aantalCEPerPallet) {
     this.aantalCEPerPallet = aantalCEPerPallet;
 }

 public Integer getMinimaleBestelhoeveelheidInCE() {
     return minimaleBestelhoeveelheidInCE;
 }

 public void setMinimaleBestelhoeveelheidInCE(Integer minimaleBestelhoeveelheidInCE) {
     this.minimaleBestelhoeveelheidInCE = minimaleBestelhoeveelheidInCE;
 }

 @Override
 public String toString() {
     return "Logistiek{" +
    		 "productNaam='" + productNaam + "'" +
             ", ceBrutoGewichtInGram='" + ceBrutoGewichtInGram + "'" +
             ", ceNettoGewichtInGram='" + ceNettoGewichtInGram + "'" +
             ", ceLengteInMm='" + ceLengteInMm + "'" +
             ", ceBreedteInMm='" + ceBreedteInMm + "'" +
             ", ceHoogteInMm='" + ceHoogteInMm + "'" +
             ", aantalCEPerHE='" + aantalCEPerHE + "'" +
             ", heEANCode='" + heEANCode + "'" +
             ", heBrutoGewichtInGram='" + heBrutoGewichtInGram + "'" +
             ", heLengteInMm='" + heLengteInMm + "'" +
             ", heBreedteInMm='" + heBreedteInMm + "'" +
             ", heHoogteInMm='" + heHoogteInMm + "'" +
             ", aantalCEPerLaag='" + aantalCEPerLaag + "'" +
             ", aantalCEPerPallet='" + aantalCEPerPallet + "'" +
             ", minimaleBestelhoeveelheidInCE='" + minimaleBestelhoeveelheidInCE + "'" +
             "}";
 }
}

