package nl.plus.hanos.extendedparser.dataobjecten;

public class GTIN {
    private String id;
    private String name;
    private String eanControleStatus;
    private String eanNr;
    private String eanType;
    private String primaireCodeIndicator;

    public GTIN() {}

    public GTIN(String id) {
        this.id = id;
    }

    public GTIN(String id, String eanNr) {
        this.id = id;
        this.eanNr = eanNr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEanControleStatus() {
        return eanControleStatus;
    }

    public void setEanControleStatus(String eanControleStatus) {
        this.eanControleStatus = eanControleStatus;
    }

    public String getEanNr() {
        return eanNr;
    }

    public void setEanNr(String eanNr) {
        this.eanNr = eanNr;
    }

    public String getEanType() {
        return eanType;
    }

    public void setEanType(String eanType) {
        this.eanType = eanType;
    }

    public String getPrimaireCodeIndicator() {
        return primaireCodeIndicator;
    }

    public void setPrimaireCodeIndicator(String primaireCodeIndicator) {
        this.primaireCodeIndicator = primaireCodeIndicator;
    }

    @Override
    public String toString() {
        return "GTIN{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", EanControleStatus='" + eanControleStatus + '\'' +
                ", EANNr='" + eanNr + '\'' +
                ", EanType='" + eanType + '\'' +
                ", PrimaireCodeIndicator='" + primaireCodeIndicator + '\'' +
                '}';
    }
}
