package nl.plus.hanos.extendedparser.dataobjecten;

import java.util.HashMap;
import java.util.Map;

//Standaard POJO voor voedingswaarde informatie over een artikel
public class VoedingswaardeObject {
    private String productNaam;
    private Integer minimaleHoudbaarheidLevering;
    private Integer minimaleHoudbaarheidProductie;
    private Double energieInKjoules;
    private Double energieInKcal;
    private Double vetten;
    private Double waarvanVerzadigd;
    private Double enkelvoudigOnverzadigd;
    private Double meervoudigOnverzadigd;
    private Double koolhydraten;
    private Double waarvanSuiker;
    private Double eiwitten;
    private Double zout;
    private Double vezels;
    private Double polyolen;
    private Double zetmeel;
    private Double cholesterol;
    private String ingredienten;
    private Integer bewaaradviesMinimaleTemp;
    private Integer bewaaradviesMaximaleTemp;
    private String bewaaradviesNaOpenen;
    private String bereidingsadvies;

    public VoedingswaardeObject(String productNaam, Integer minimaleHoudbaarheidLevering, Integer minimaleHoudbaarheidProductie, Double energieInKjoules, Double energieInKcal, Double vetten, Double waarvanVerzadigd, Double enkelvoudigOnverzadigd, Double meervoudigOnverzadigd, Double koolhydraten, Double waarvanSuiker, Double eiwitten, Double zout, Double vezels, Double polyolen, Double zetmeel, Double cholesterol, String ingredienten, Integer bewaaradviesMinimaleTemp, Integer bewaaradviesMaximaleTemp, String bewaaradviesNaOpenen, String bereidingsadvies) {
        this.productNaam = productNaam;
        this.minimaleHoudbaarheidLevering = minimaleHoudbaarheidLevering;
        this.minimaleHoudbaarheidProductie = minimaleHoudbaarheidProductie;
        this.energieInKjoules = energieInKjoules;
        this.energieInKcal = energieInKcal;
        this.vetten = vetten;
        this.waarvanVerzadigd = waarvanVerzadigd;
        this.enkelvoudigOnverzadigd = enkelvoudigOnverzadigd;
        this.meervoudigOnverzadigd = meervoudigOnverzadigd;
        this.koolhydraten = koolhydraten;
        this.waarvanSuiker = waarvanSuiker;
        this.eiwitten = eiwitten;
        this.zout = zout;
        this.vezels = vezels;
        this.polyolen = polyolen;
        this.zetmeel = zetmeel;
        this.cholesterol = cholesterol;
        this.ingredienten = ingredienten;
        this.bewaaradviesMinimaleTemp = bewaaradviesMinimaleTemp;
        this.bewaaradviesMaximaleTemp = bewaaradviesMaximaleTemp;
        this.bewaaradviesNaOpenen = bewaaradviesNaOpenen;
        this.bereidingsadvies = bereidingsadvies;
    }

    public String getProductNaam() {
        return productNaam;
    }

    public void setProductNaam(String productNaam) {
        this.productNaam = productNaam;
    }

    public Integer getMinimaleHoudbaarheidLevering() {
        return minimaleHoudbaarheidLevering;
    }

    public void setMinimaleHoudbaarheidLevering(Integer minimaleHoudbaarheidLevering) {
        this.minimaleHoudbaarheidLevering = minimaleHoudbaarheidLevering;
    }

    public Integer getMinimaleHoudbaarheidProductie() {
        return minimaleHoudbaarheidProductie;
    }

    public void setMinimaleHoudbaarheidProductie(Integer minimaleHoudbaarheidProductie) {
        this.minimaleHoudbaarheidProductie = minimaleHoudbaarheidProductie;
    }

    public Double getEnergieInKjoules() {
        return energieInKjoules;
    }

    public void setEnergieInKjoules(Double energieInKjoules) {
        this.energieInKjoules = energieInKjoules;
    }

    public Double getEnergieInKcal() {
        return energieInKcal;
    }

    public void setEnergieInKcal(Double energieInKcal) {
        this.energieInKcal = energieInKcal;
    }

    public Double getVetten() {
        return vetten;
    }

    public void setVetten(Double vetten) {
        this.vetten = vetten;
    }

    public Double getWaarvanVerzadigd() {
        return waarvanVerzadigd;
    }

    public void setWaarvanVerzadigd(Double waarvanVerzadigd) {
        this.waarvanVerzadigd = waarvanVerzadigd;
    }

    public Double getEnkelvoudigOnverzadigd() {
        return enkelvoudigOnverzadigd;
    }

    public void setEnkelvoudigOnverzadigd(Double enkelvoudigOnverzadigd) {
        this.enkelvoudigOnverzadigd = enkelvoudigOnverzadigd;
    }

    public Double getMeervoudigOnverzadigd() {
        return meervoudigOnverzadigd;
    }

    public void setMeervoudigOnverzadigd(Double meervoudigOnverzadigd) {
        this.meervoudigOnverzadigd = meervoudigOnverzadigd;
    }

    public Double getKoolhydraten() {
        return koolhydraten;
    }

    public void setKoolhydraten(Double koolhydraten) {
        this.koolhydraten = koolhydraten;
    }

    public Double getWaarvanSuiker() {
        return waarvanSuiker;
    }

    public void setWaarvanSuiker(Double waarvanSuiker) {
        this.waarvanSuiker = waarvanSuiker;
    }

    public Double getEiwitten() {
        return eiwitten;
    }

    public void setEiwitten(Double eiwitten) {
        this.eiwitten = eiwitten;
    }

    public Double getZout() {
        return zout;
    }

    public void setZout(Double zout) {
        this.zout = zout;
    }

    public Double getVezels() {
        return vezels;
    }

    public void setVezels(Double vezels) {
        this.vezels = vezels;
    }

    public Double getPolyolen() {
        return polyolen;
    }

    public void setPolyolen(Double polyolen) {
        this.polyolen = polyolen;
    }

    public Double getZetmeel() {
        return zetmeel;
    }

    public void setZetmeel(Double zetmeel) {
        this.zetmeel = zetmeel;
    }

    public Double getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(Double cholesterol) {
        this.cholesterol = cholesterol;
    }

    public String getIngredienten() {
        return ingredienten;
    }

    public void setIngredienten(String ingredienten) {
        this.ingredienten = ingredienten;
    }

    public Integer getBewaaradviesMinimaleTemp() {
        return bewaaradviesMinimaleTemp;
    }

    public void setBewaaradviesMinimaleTemp(Integer bewaaradviesMinimaleTemp) {
        this.bewaaradviesMinimaleTemp = bewaaradviesMinimaleTemp;
    }

    public Integer getBewaaradviesMaximaleTemp() {
        return bewaaradviesMaximaleTemp;
    }

    public void setBewaaradviesMaximaleTemp(Integer bewaaradviesMaximaleTemp) {
        this.bewaaradviesMaximaleTemp = bewaaradviesMaximaleTemp;
    }

    public String getBewaaradviesNaOpenen() {
        return bewaaradviesNaOpenen;
    }

    public void setBewaaradviesNaOpenen(String bewaaradviesNaOpenen) {
        this.bewaaradviesNaOpenen = bewaaradviesNaOpenen;
    }

    public String getBereidingsadvies() {
        return bereidingsadvies;
    }

    public void setBereidingsadvies(String bereidingsadvies) {
        this.bereidingsadvies = bereidingsadvies;
    }

    @Override
    public String toString() {
        return "VoedingswaardeObject{" +
                "productNaam='" + productNaam + '\'' +
                ", minimaleHoudbaarheidLevering=" + minimaleHoudbaarheidLevering +
                ", minimaleHoudbaarheidProductie=" + minimaleHoudbaarheidProductie +
                ", energieInKjoules=" + energieInKjoules +
                ", energieInKcal=" + energieInKcal +
                ", vetten=" + vetten +
                ", waarvanVerzadigd=" + waarvanVerzadigd +
                ", enkelvoudigOnverzadigd=" + enkelvoudigOnverzadigd +
                ", meervoudigOnverzadigd=" + meervoudigOnverzadigd +
                ", koolhydraten=" + koolhydraten +
                ", waarvanSuiker=" + waarvanSuiker +
                ", eiwitten=" + eiwitten +
                ", zout=" + zout +
                ", vezels=" + vezels +
                ", polyolen=" + polyolen +
                ", zetmeel=" + zetmeel +
                ", cholesterol=" + cholesterol +
                ", ingredienten='" + ingredienten + '\'' +
                ", bewaaradviesMinimaleTemp=" + bewaaradviesMinimaleTemp +
                ", bewaaradviesMaximaleTemp=" + bewaaradviesMaximaleTemp +
                ", bewaaradviesNaOpenen='" + bewaaradviesNaOpenen + '\'' +
                ", bereidingsadvies='" + bereidingsadvies + '\'' +
                '}';
    }
}

