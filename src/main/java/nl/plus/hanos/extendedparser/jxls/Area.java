package nl.plus.hanos.extendedparser.jxls;

import org.jxls.common.*;
import org.jxls.common.cellshift.CellShiftStrategy;

import java.util.List;

/**
 * Generic interface for excel area processing
 * 
 * @author Leonid Vysochyn
 * @since 1/18/12
 */
public interface Area {

    Size applyAt(CellRef cellRef, Context context);

    CellRef getStartCellRef();
    
    Size getSize();
    
    AreaRef getAreaRef();

    List<CommandData> getCommandDataList();

    CellShiftStrategy getCellShiftStrategy();

    void setCellShiftStrategy(CellShiftStrategy cellShiftStrategy);

    FormulaProcessor getFormulaProcessor();

    void setFormulaProcessor(FormulaProcessor formulaProcessor);

    void addCommand(AreaRef ref, Command command);

    Transformer getTransformer();
    
    void processFormulas();
    
    void addAreaListener(AreaListener listener);
    
    List<AreaListener> getAreaListeners();

    List<Command> findCommandByName(String name);

    void reset();

    Command getParentCommand();
    
    void setParentCommand(Command command);
}
