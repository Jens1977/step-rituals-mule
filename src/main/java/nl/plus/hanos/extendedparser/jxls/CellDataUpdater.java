package nl.plus.hanos.extendedparser.jxls;

import org.jxls.command.UpdateCellCommand;
import org.jxls.common.CellRef;
import org.jxls.common.Context;

/**
 * Interface for updating {@link CellData}
 * 
 * @see UpdateCellCommand
 */
public interface CellDataUpdater {

    void updateCellData(CellData cellData, CellRef targetCell, Context context);
}
