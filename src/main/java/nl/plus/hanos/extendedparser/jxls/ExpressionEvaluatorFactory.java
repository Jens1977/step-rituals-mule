package nl.plus.hanos.extendedparser.jxls;

public interface ExpressionEvaluatorFactory {

	ExpressionEvaluator createExpressionEvaluator(String expression);
}
