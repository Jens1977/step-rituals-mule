package nl.plus.hanos.extendedparser.jxls;

import org.apache.poi.ss.usermodel.Row;
import org.jxls.common.CellRef;

/**
 * Row data wrapper for POI row
 * 
 * @author Leonid Vysochyn
 * @since 2/1/12
 */
public class PoiRowData extends RowData {
    private Row row;

    public static RowData createRowData(Row row, PoiTransformer transformer) {
        if (row == null) {
            return null;
        }
        PoiRowData rowData = new PoiRowData();
        rowData.setTransformer(transformer);
        rowData.row = row;
        rowData.height = row.getHeight();
        int numberOfCells = row.getLastCellNum();
        for (int cellIndex = 0; cellIndex < numberOfCells; cellIndex++) {
            org.apache.poi.ss.usermodel.Cell cell = row.getCell(cellIndex);
            if (cell != null) {
                CellData cellData = PoiCellData.createCellData(new CellRef(row.getSheet().getSheetName(), row.getRowNum(), cellIndex), cell);
                cellData.setTransformer(transformer);
                rowData.addCellData(cellData);
            } else {
                rowData.addCellData(null);
            }
        }
        return rowData;
    }

    public Row getRow() {
        return row;
    }
}
