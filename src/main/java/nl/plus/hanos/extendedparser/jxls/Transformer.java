package nl.plus.hanos.extendedparser.jxls;

import org.jxls.common.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Defines interface methods for Excel operations
 *
 * @author Leonid Vysochyn
 * @since 1/23/12
 */
public interface Transformer {

    void setTransformationConfig(TransformationConfig transformationConfig);

    TransformationConfig getTransformationConfig();

    void transform(CellRef srcCellRef, CellRef targetCellRef, Context context, boolean updateRowHeight);

    /**
     * Writes Excel workbook to output stream and disposes the workbook.
     * 
     * @throws IOException
     */
    void write() throws IOException;
    
    /**
     * Must be called after use. write() calls this method.
     */
    void dispose();

    void setFormula(CellRef cellRef, String formulaString);

    Set<CellData> getFormulaCells();

    CellData getCellData(CellRef cellRef);

    /**
     * @param cellRef a source cell reference
     * @return a list of cell references into which the source cell was transformed
     */
    List<CellRef> getTargetCellRef(CellRef cellRef);

    void resetTargetCellRefs();

    void resetArea(AreaRef areaRef);

    void clearCell(CellRef cellRef);

    List<CellData> getCommentedCells();

    void addImage(AreaRef areaRef, byte[] imageBytes, ImageType imageType);

    boolean deleteSheet(String sheetName);

    void setHidden(String sheetName, boolean hidden);

    void updateRowHeight(String srcSheetName, int srcRowNum, String targetSheetName, int targetRowNum);

    void adjustTableSize(CellRef ref, Size size);

    void mergeCells(CellRef ref, int rows, int cols);
}
