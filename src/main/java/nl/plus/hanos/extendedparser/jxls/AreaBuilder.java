package nl.plus.hanos.extendedparser.jxls;


import java.util.List;

public interface AreaBuilder {

    List<Area> build();

    void setTransformer(Transformer transformer);

    Transformer getTransformer();
}
