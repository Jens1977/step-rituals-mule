package nl.plus.hanos.extendedparser.jxls;

import java.util.Map;

/**
 * An interface to evaluate expressions
 * 
 * @author Leonid Vysochyn
 * @since 11/02/2009
 */
public interface ExpressionEvaluator {

    Object evaluate(String expression, Map<String, Object> context);
    
    Object evaluate(Map<String, Object> context);
    
    String getExpression();
}
